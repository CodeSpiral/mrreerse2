# 安装
```sh
git clone https://gitlab.com/CodeSpiral/mrreerse2.git
cd mrreerse2
npm install --production
```
> 然后放好证书到 `crt` 文件夹（自行创建文件夹： `mkdir crt` ）

默认读取证书： `YuanHuanJiLu-pri.crt` 和 `YuanHuanJiLu-pri.key` 

> 证书要先创建CA证书，再创建域名用证书。
> CA证书装到手机里。

# 运行
> 首次运行自动创建默认配置文件 `config.json` 。
```sh
# 运行
node out/app.js
```

# 配置
> 建议先运行创建默认配置文件后，再配置文件，然后重启即可。


## config.json配置文件
缓存数据包
```
CacheDownloadPack: true
```

发送到数据收集服务器
```
SendToCollectionServer: true,
CollectionServer: "服务器IP",
CollectionServerPort: 服务器端口,
```
例子
```
SendToCollectionServer: true,
CollectionServer: "127.0.0.1",
CollectionServerPort: 1551,
```

多用户模式（不稳定）
```
ReerseMultiUser: true,
```