"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Path = require("path");
const _Fs = require("fs");
const _Colors = require("colors");
const _ConfigFile = 'config.json';
const _ConfigPath = _Path.join(__dirname, '..');
exports._IsDev = (process.argv[2] === 'dev') || (__dirname.lastIndexOf('app') === (__dirname.length - 3));
if (exports._IsDev)
    console.log(_Colors.bgCyan.blue(' 开发模式 '));
let _OtherCmdArg = [];
function PArgsSetConfig() {
    let cmds = [];
    for (let index = 2; index < process.argv.length; index++) {
        const cmd = process.argv[index];
        if (cmd == 'dev') { }
        else {
            cmds.push(cmd);
        }
    }
    if (cmds && cmds.length) {
        if (cmds[0] && !isNaN(cmds[0])) {
            _Config.UseSocket5 = true;
            _Config.Socket5Port = parseInt(cmds[0]);
            cmds.shift();
        }
    }
    for (let index = 0; index < cmds.length; index++) {
        _OtherCmdArg.push(cmds[index].toUpperCase());
    }
}
function HasCmdArg(name, flag) {
    name = name.toUpperCase();
    var i = _OtherCmdArg.indexOf(name);
    if (i >= 0) {
        if (flag == 'notDel')
            return true;
        _OtherCmdArg.splice(i, 1);
        return true;
    }
    return false;
}
exports.HasCmdArg = HasCmdArg;
function HasCmdArg2Num(name, notDel) {
    name = name.toUpperCase();
    var i = _OtherCmdArg.indexOf(name);
    if (i >= 0) {
        let r = _OtherCmdArg[i + 1];
        let num = parseInt(r);
        console.log(`${name}: ${r} -> ${num} `);
        if (notDel)
            return num;
        _OtherCmdArg.splice(i, 2);
        console.log(_OtherCmdArg);
        return num;
    }
    return false;
}
exports.HasCmdArg2Num = HasCmdArg2Num;
let _Config = {
    DomainConf: [
        {
            use: true,
            cert: 'YuanHuanJiLu-pri',
            domain: [
                'android.magi-reco.com',
                'ios.magi-reco.com',
            ],
        },
    ],
    ReerseServer: true,
    ReerseServerPort: 443,
    ReerseMultiUser: false,
    ReerseServerLog: false,
    UseSocket5: false,
    Socket5Port: 8889,
    Socket5Log: false,
    SendToCollectionServer: false,
    CollectionServer: '127.0.0.1',
    CollectionServerPort: 15511,
    CollectionQuestBattleGet: true,
    CacheFileAll: false,
    CacheFileImage: false,
    CacheDownloadPack: false,
    SaveAllApiRequsetBody: false,
    SaveAllApiResponseBody: false,
    UseLocDownloadPack: true,
    LoadAsset: true,
    LocDownloadPackServerMode: false,
    Cache304ETag: false,
    ConfigLog: true,
    DetailLog: false,
    ConsoleCmd: true,
};
if (_Fs.existsSync(_Path.join(_ConfigPath, _ConfigFile))) {
    const c = ReadJsonFileSync(_ConfigPath, _ConfigFile);
    if (c) {
        for (const key in _Config) {
            if (c.hasOwnProperty(key)) {
                let value = c[key];
                if (typeof value === 'boolean')
                    _Config[key] = (c[key] ? true : false);
                else
                    _Config[key] = c[key];
            }
        }
        if (exports._IsDev && c['DevConfigSet']) {
            var dcs = c['DevConfigSet'];
            for (const key in _Config) {
                if (dcs.hasOwnProperty(key)) {
                    let value = c[key];
                    let valueDev = dcs[key];
                    if (value === valueDev)
                        continue;
                    if (typeof value === 'boolean')
                        valueDev = (valueDev ? true : false);
                    _Config[key] = valueDev;
                    if (value !== undefined)
                        console.log(`DEV覆盖配置：${key.blue}: `, value, ` -> `, valueDev);
                }
            }
        }
    }
}
else {
    WriteFileJson(_ConfigPath, _ConfigFile, _Config, false);
}
PArgsSetConfig();
if (HasCmdArg('NR')) {
    _Config.ReerseServer = false;
}
let rReerseServerPort = HasCmdArg2Num('LP');
if (rReerseServerPort) {
    _Config.ReerseServerPort = rReerseServerPort;
}
if (HasCmdArg('SC')) {
    _Config.SendToCollectionServer = true;
}
if (_Config.UseLocDownloadPack)
    _Config.CacheDownloadPack = true;
if (HasCmdArg('LocDownloadPackServerMode')) {
    _Config.LocDownloadPackServerMode = true;
}
if (_Config.LocDownloadPackServerMode) {
    _Config.LoadAsset = true;
    _Config.UseLocDownloadPack = true;
    _Config.CacheDownloadPack = true;
}
if (_Config.ConfigLog) {
    console.log('配置: '.cyan, _Config);
}
function InitDomainConf() {
    for (let index = 0; index < _Config.DomainConf.length; index++) {
        const item = _Config.DomainConf[index];
        if (item.use) {
            const cert = _Path.join(__dirname, '../crt', (item.cert + '.crt'));
            const certKey = _Path.join(__dirname, '../crt', (item.cert + '.key'));
            if (_Fs.existsSync(cert) == false) {
                console.log(`证书: Crt => ${cert} 文件不存在！`.red.bgYellow);
            }
            if (_Fs.existsSync(certKey) == false) {
                console.log(`证书: Key => ${certKey} 文件不存在！`.red.bgYellow);
            }
            item.certPem = _Fs.readFileSync(cert, 'utf8');
            item.certKeyPem = _Fs.readFileSync(certKey, 'utf8');
        }
    }
}
const Conf = {
    get DomainConf() {
        InitDomainConf();
        return _Config.DomainConf;
    },
    get ReerseServer() { return _Config.ReerseServer; },
    get ReerseServerPort() { return _Config.ReerseServerPort; },
    SetReerseServerPort(port) { _Config.ReerseServerPort = port; },
    get ReerseMultiUser() { return _Config.ReerseMultiUser; },
    get ReerseServerLog() { return _Config.ReerseServerLog; },
    get UseSocket5() { return _Config.UseSocket5; },
    get Socket5Port() { return _Config.Socket5Port; },
    get Socket5Log() { return _Config.Socket5Log; },
    get SendToCollectionServer() { return _Config.SendToCollectionServer; },
    SendToCollectionServer_Stop() { _Config.SendToCollectionServer = false; },
    get CollectionServerIp() { return _Config.CollectionServer; },
    get CollectionServerPort() { return _Config.CollectionServerPort; },
    get CollectionQuestBattleGet() { return _Config.CollectionQuestBattleGet; },
    get CacheFileImage() { return _Config.CacheFileImage; },
    get CacheFileAll() { return _Config.CacheFileAll; },
    get CacheDownloadPack() { return _Config.CacheDownloadPack; },
    get SaveAllApiRequsetBody() { return _Config.SaveAllApiRequsetBody; },
    get SaveAllApiResponseBody() { return _Config.SaveAllApiResponseBody; },
    get UseLocDownloadPack() { return _Config.UseLocDownloadPack; },
    get LoadAsset() { return _Config.LoadAsset; },
    get LocDownloadPackServerMode() { return _Config.LocDownloadPackServerMode; },
    get Cache304ETag() { return _Config.Cache304ETag; },
    get DetailLog() { return _Config.DetailLog; },
    get ConfigLog() { return _Config.ConfigLog; },
};
exports.Conf = Conf;
var Dir;
(function (Dir) {
    Dir._Work = _Path.join(__dirname, '..');
    const _File = exports._IsDev ? _Path.join(__dirname, '../..') : Dir._Work;
    Dir._FileMagica = _Path.join(_File, 'magica');
    Dir._FileMagicaExtend = _Path.join(Dir._FileMagica, 'UserExtend');
    Dir._FileMagicaExtend_Api = _Path.join(Dir._FileMagica, 'UserExtend/Api');
})(Dir = exports.Dir || (exports.Dir = {}));
function ReadJsonFileSync(dir, fileName) {
    let file = _Path.join(dir, fileName);
    if (_Fs.existsSync(file)) {
        try {
            let txt = _Fs.readFileSync(file, { encoding: 'utf8' });
            return JSON.parse(txt);
        }
        catch (error) {
            console.warn(`[读]JSON失败！: ${file} `.red, error);
            return undefined;
        }
    }
    return undefined;
}
function WriteFileJson(dir, fileName, obj, log) {
    WriteFile(dir, fileName, JSON.stringify(obj, null, 4), log);
}
function WriteFile(dir, fileName, data, log) {
    try {
        let file = _Path.join(dir, fileName);
        let ws = _Fs.createWriteStream(file).on('error', function (err) {
            console.warn('[写][Error]:'.red, err);
        }).end(data, function () {
            if (log)
                console.log('[写]: ' + file);
        });
    }
    catch (error) {
        console.warn('[写][Error]:'.red, error);
    }
}
const _Cmds = require("./lib/cmd");
_Cmds.Add([
    {
        cmd: ['c', 'config'],
        txt: '当前配置信息。', func: () => {
            console.log('配置: '.cyan, _Config);
            console.log('工作目录:  '.cyan, Dir._Work);
            console.log('文件目录1: '.cyan, Dir._FileMagica);
            console.log('文件目录2: '.cyan, Dir._FileMagicaExtend);
            if (_Config.ReerseMultiUser) {
                console.log('当前：多用户模式');
            }
            if (_Config.LocDownloadPackServerMode) {
                console.log('当前：数据包服务器模式（DEV）（未具体测试）');
            }
        },
    },
    {
        cmd: ['stcs'],
        txt: '临时改变配置： SendToCollectionServer ', func: () => {
            console.log(`  SendToCollectionServer: `, (_Config.SendToCollectionServer = !_Config.SendToCollectionServer));
        },
    },
    {
        cmd: ['ss'],
        txt: '临时改变配置： UseSocket5 ', func: () => {
            console.log(`  UseSocket5: `, (_Config.UseSocket5 = !_Config.UseSocket5));
            console.log(`  Socket5Port: `, _Config.Socket5Port);
        },
    },
    {
        cmd: ['saa', 'ass'],
        txt: '临时改变配置： SaveAllApi[Response|Requset]Body （记录请求&响应）', func: () => {
            console.log(`  SaveAllApiResponseBody: `, (_Config.SaveAllApiResponseBody = !_Config.SaveAllApiResponseBody));
            console.log(`  SaveAllApiRequsetBody: `, (_Config.SaveAllApiRequsetBody = !_Config.SaveAllApiRequsetBody));
        },
    },
]);
exports.SetCmdsExt = _Cmds.Add;
_Cmds.Start(true);
