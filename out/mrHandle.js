"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Path = require("path");
const _Fs = require("fs");
const _Crypto = require("crypto");
const _App = require("./config");
const _AppC = _App.Conf;
const _SendCollection = require("./sendCollection");
const _TAG_ = 'MRH';
const _LOG_ = _AppC.DetailLog || _App._IsDev;
const _FileBaseMagica = _App.Dir._FileMagica;
const _FileMagicaExtend_Api = _App.Dir._FileMagicaExtend_Api;
const _DownloadPackUrl_Src = '/magica/resource/download/asset/master/resource/';
const _DownloadPackUrl_Loc = '/magica/resource/downloadPack/';
const _UrlDisc = {
    '/magica/resource/download/asset/master/resource/image_native/art/': 18,
    '/magica/resource/download/asset/master/resource/image_native/card/': 18,
    '/magica/resource/download/asset/master/resource/image_native/gift/': 18,
    '/magica/resource/download/asset/master/resource/image_native/item/': 18,
    '/magica/resource/download/asset/master/resource/image_native/memoria/': 18,
    '/magica/resource/download/asset/master/resource/scenario/json/adv/': 17,
    '/magica/resource/download/asset/master/resource/': 20,
    '/magica/resource/download/asset/master/': 19,
    '/magica/resource/image_web/item/': 9,
    '/magica/resource/image_web/': 10,
    '/magica/api/': 51,
    '/search/friend_search/': 51,
    '/magica/': 10,
    '/favicon.ico': 10,
    '': 99,
};
function GetUrlFileType(pathname) {
    for (const key in _UrlDisc) {
        if (pathname.indexOf(key) === 0) {
            return _UrlDisc[key];
        }
    }
    return 99;
}
function AsyncMrHandle(mu) {
    const resBody = mu._res.decodedBody;
    const statusCode = mu._res.statusCode;
    if (statusCode == 304) {
        StatusCode304Cache(mu._req.urlPath, mu._req.headers, mu._res.headers);
        return;
    }
    if (statusCode != 200) {
        _LOG_ && console.log(`[${_TAG_}][AsyncMrHandle]响应: ${statusCode} != 200 不处理.`.yellow, resBody && resBody.toString());
        return;
    }
    if (resBody == undefined) {
        _LOG_ && console.log(`[${_TAG_}]AsyncMrHandle: not do. (resBody null)`.yellow);
        return;
    }
    if (resBody.length > 0) {
        setTimeout(() => {
            DoMrHandle(mu._req.urlPathSrc, mu._req.urlPathParse, mu._req.headers, mu._req.GetDecodedBody(), mu._res.headers, resBody, mu.reqMethod);
        }, 0);
    }
    else {
        _LOG_ && console.log(`[${_TAG_}]AsyncMrHandle: not do. (resBody null)`.yellow);
        return;
    }
}
exports.AsyncMrHandle = AsyncMrHandle;
function DoMrHandle(urlStr, url, headers, bodySrc, resHeaders, resBody, reqMethod) {
    const pathname = url.pathname || '';
    const pathParse = _Path.parse(pathname);
    const filePath = pathParse.dir;
    const fileName = pathParse.base;
    let ufType = GetUrlFileType(pathname);
    var canSaveFile = false;
    var isDownloadPack = (ufType > 10 && ufType <= 20);
    if (_AppC.CacheFileAll && ufType <= 20)
        canSaveFile = true;
    else if (_AppC.CacheDownloadPack && isDownloadPack)
        canSaveFile = true;
    else if (_AppC.CacheFileImage && (ufType == 9 || ufType == 18))
        canSaveFile = true;
    if (canSaveFile) {
        const savePath = _Path.posix.join(_FileBaseMagica, (isDownloadPack ? filePath.replace(_DownloadPackUrl_Src, _DownloadPackUrl_Loc) : filePath));
        const saveFileName = fileName.replace('.json.gz', '.json');
        if (pathname === '/magica/index.html' && resBody.length < 512) {
            console.log('[index.html]: ', resBody.toString());
            console.log('不保存该index.html');
            return;
        }
        SaveFile(true, savePath, saveFileName, resBody, true, true);
        if (fileName.indexOf('asset_') === 0) {
            try {
                SetAssetJson(fileName, JSON.parse(resBody.toString()));
            }
            catch (error) {
                console.log(`[${_TAG_}]SetAssetJson: ${fileName}`.red, error);
            }
        }
        if (isDownloadPack) {
            setTimeout(() => {
                DownloadPackMerge(ufType, _Path.posix.join(savePath + '/' + saveFileName));
            }, 1);
        }
    }
    if (ufType == 51) {
        if (_AppC.SaveAllApiRequsetBody && bodySrc) {
            if (false == (reqMethod == 'GET' && bodySrc.length == 0)) {
                let saveFileName = `${pathParse.name}_Req` + (pathParse.ext ? pathParse.ext : '.json');
                SaveFile(false, _Path.posix.join(_FileMagicaExtend_Api, filePath), saveFileName, bodySrc, true);
            }
            try {
                let saveFileNameH = `${pathParse.name}_Head` + (pathParse.ext ? pathParse.ext : '.json');
                SaveFile(false, _Path.posix.join(_FileMagicaExtend_Api, filePath), saveFileNameH, JSON.stringify({
                    method: reqMethod,
                    req: headers,
                    res: resHeaders
                }), true);
            }
            catch (error) {
                console.log(`[${_TAG_}][Save:${pathParse.name}_Head]error`.magenta, error);
            }
        }
        if (_AppC.SaveAllApiResponseBody) {
            let saveFileName = pathParse.name + (pathParse.ext ? pathParse.ext : '.json');
            SaveFile(false, _Path.posix.join(_FileMagicaExtend_Api, filePath), saveFileName, resBody, true);
        }
        try {
            _SendCollection.CheckSend_Something(urlStr, url, resBody);
        }
        catch (error) {
            console.log(`[${_TAG_}]error`.magenta, error);
        }
    }
    if (ufType == 99) {
        console.log(`[${_TAG_}]未识别的URL：${pathname}`.magenta);
    }
}
const _SaveFile_C_Disc = {};
_App.SetCmdsExt([
    {
        cmd: ['c-fd'],
        txt: '当前路径状态信息（文件缓存内容）。', func: () => {
            console.log('路径状态：', _SaveFile_C_Disc);
        },
    }
]);
function SaveFile(checkMD5, path, name, data, log, sync) {
    if (!data) {
        return;
    }
    const file = _Path.posix.join(path, name);
    let thisMD5 = '';
    if (checkMD5) {
        thisMD5 = MD5(data);
        if (_SaveFile_C_Disc[file]) {
            const md5 = _SaveFile_C_Disc[file];
            if (typeof md5 === 'string') {
                if (md5 === thisMD5) {
                    return;
                }
            }
            else {
                console.warn(`[${_TAG_}][SF:Error]md5不是字符串！:`.yellow, file);
            }
        }
        else {
            if (_Fs.existsSync(file)) {
                const oldMD5 = MD5ofFile(file);
                if (thisMD5 === oldMD5) {
                    _SaveFile_C_Disc[file] = thisMD5;
                    return;
                }
            }
        }
    }
    if (!_SaveFile_C_Disc[path]) {
        CreateDirsSync(path);
        _SaveFile_C_Disc[path] = true;
    }
    if (checkMD5) {
        _SaveFile_C_Disc[file] = thisMD5;
    }
    if (file.indexOf(_FileBaseMagica) !== 0) {
        console.log(`[${_TAG_}][SF:Error]不是指定的保存路径！`.magenta, file);
        return;
    }
    if (sync) {
        WriteFileSync(file, data, log);
    }
    else {
        WriteFile(file, data, log);
    }
}
const _Asset_C_Disc = {};
function SetAssetJson(name, json) {
    if (name.indexOf('asset_config') === 0)
        return;
    const returnNotLog = (name.indexOf('asset_scenario_') === 0);
    let isOneNum = 0;
    let okNum = 0;
    let okBUGNum = 0;
    let okMovieNum = 0;
    for (let index = 0; index < json.length; index++) {
        const asset = json[index];
        if (asset && asset.path && asset.file_list) {
            if (asset.file_list.length == 1) {
                isOneNum++;
                if (true) {
                    const assetpath = asset.path;
                    const nm = assetpath.match(_Reg_DownloadPackNameAAA);
                    const fileNameAAA = (nm && nm[1]) ? nm[1] : '';
                    if (fileNameAAA) {
                        okBUGNum++;
                        _Asset_C_Disc[asset.path] = asset;
                    }
                }
            }
            else if (asset.file_list[0].url.indexOf(asset.path) !== 0) {
                if (asset.file_list[0].url.indexOf('movie/char/high/') === 0
                    || asset.file_list[0].url.indexOf('movie/dmm/') === 0) {
                    let path = asset.file_list[0].url;
                    const nm = path.match(_Reg_DownloadPackNameAAA);
                    const fileNameAAA = (nm && nm[1]) ? nm[1] : '';
                    path = fileNameAAA ? path.slice(0, -4) : path;
                    _Asset_C_Disc[path] = asset;
                    okMovieNum++;
                }
                else {
                    console.log(`[${_TAG_}]异常Asset(文件？)为符合当前的处理规则: ${name}`.magenta, asset);
                }
            }
            else {
                _Asset_C_Disc[asset.path] = asset;
                okNum++;
            }
        }
        else {
            console.log(`[${_TAG_}]异常Asset: ${name}`.magenta);
            return;
        }
    }
    if (returnNotLog && isOneNum == json.length) {
        return;
    }
    let log = `[${_TAG_}][${name}]载入分割文件数: ${okNum}`;
    if (okMovieNum)
        log += `+${okMovieNum}个视频文件`;
    log += ` (总:${json.length},单:${isOneNum})`;
    log = log.cyan;
    if (okBUGNum)
        log += `(B文件数:${okBUGNum})`.magenta;
    console.log(log);
}
let isDo_LoadLocAssetJson = false;
function LoadLocAssetJson() {
    isDo_LoadLocAssetJson = true;
    const pathDir = _Path.join(_FileBaseMagica, `/magica/resource/download/asset/master/`);
    const list = [];
    const files = _Fs.readdirSync(pathDir);
    files.forEach(function (item, index) {
        let stat = _Fs.lstatSync(_Path.join(pathDir, item));
        if (stat.isFile() === true
            && item.indexOf('asset_config.json') === -1
            && item.indexOf('asset_scenario_') === -1
            && item.indexOf('asset_section_') === -1) {
            list.push(item);
        }
    });
    for (let index = 0; index < list.length; index++) {
        const file = list[index];
        const pathFile = _Path.join(pathDir, file);
        if (_Fs.existsSync(pathFile)) {
            try {
                const json = JSON.parse(_Fs.readFileSync(pathFile).toString());
                SetAssetJson(file, json);
            }
            catch (error) {
                console.log(`[${_TAG_}]本地Asset文件加载异常：`.magenta, pathFile);
            }
        }
        else {
            _AppC.DetailLog && console.log(`[${_TAG_}]本地Asset文件不存在：`.gray, pathFile);
        }
    }
}
exports.LoadLocAssetJson = LoadLocAssetJson;
if (_AppC.UseLocDownloadPack && _AppC.LoadAsset) {
    setTimeout(() => {
        LoadLocAssetJson();
    }, 1);
}
;
function GetAssetData(pathName, pathNameNotAAA) {
    let delIndex = pathName.indexOf(_DownloadPackUrl_Loc);
    if (delIndex === -1) {
        console.log(`[${_TAG_}-GetAssetData]异常：不匹配数据包本地缩短路径 .`.magenta, pathName);
        return undefined;
    }
    delIndex += _DownloadPackUrl_Loc.length;
    const assetFileName = pathName.substring(delIndex);
    const assetFileNameNotAAA = pathNameNotAAA.substring(delIndex);
    if (_Asset_C_Disc[assetFileNameNotAAA]) {
        return {
            asset: _Asset_C_Disc[assetFileNameNotAAA],
            fileName: assetFileName,
            fileNameNotAAA: assetFileNameNotAAA,
        };
    }
    else {
        return undefined;
    }
}
function GetAssetFileStartIndexSize(pathName, pathNameNotAAA) {
    const asset = GetAssetData(pathName, pathNameNotAAA);
    if (!asset)
        return;
    let fileStart = 0;
    for (let index = 0; index < asset.asset.file_list.length; index++) {
        const item = asset.asset.file_list[index];
        if (asset.fileName != item.url) {
            fileStart += item.size;
        }
        else {
            return {
                startIndex: fileStart,
                size: item.size,
            };
        }
    }
    console.log(`[${_TAG_}-AssetIndexSize]异常：asset内容不匹配`.magenta, asset);
    return undefined;
}
const _Reg_DownloadPackNameAAA = /\.[a-zA-Z0-9]{1,}(\.[a-z]{3})$/;
function LocDownloadPackUrlHandle(mu) {
    const pathname = mu._req.urlPath;
    let ufType = GetUrlFileType(pathname);
    if (ufType === 19)
        return;
    if (ufType <= 10 || ufType > 20)
        return;
    const md5 = mu._req.urlPathParse.query;
    if (!md5)
        return;
    const locPathFileName = _Path.posix.join(_FileBaseMagica, pathname.replace(_DownloadPackUrl_Src, _DownloadPackUrl_Loc));
    const nm = locPathFileName.match(_Reg_DownloadPackNameAAA);
    const fileNameAAA = (nm && nm[1]) ? nm[1] : '';
    const locPathFileNameNotAAA = fileNameAAA ? locPathFileName.slice(0, -4) : locPathFileName;
    let locMD5 = '';
    let locFileBuffer = undefined;
    if (_SaveFile_C_Disc[locPathFileNameNotAAA] && typeof _SaveFile_C_Disc[locPathFileNameNotAAA] === 'string') {
        locMD5 = _SaveFile_C_Disc[locPathFileNameNotAAA];
        if (locMD5 !== md5) {
            let asset = GetAssetData(locPathFileName, locPathFileName);
            if (asset && asset.asset.file_list.length == 1 && asset.asset.md5 == md5) {
                console.log(`[${_TAG_}]Asset(BUG文件)MD5修改: ${md5} -> `.blue, locMD5);
                locMD5 = md5;
            }
        }
    }
    if (!locMD5) {
        if (_Fs.existsSync(locPathFileNameNotAAA)) {
            if (!fileNameAAA) {
                locFileBuffer = _Fs.readFileSync(locPathFileNameNotAAA);
                locMD5 = MD5(locFileBuffer);
            }
            else {
                locMD5 = MD5ofBigFile(locPathFileNameNotAAA);
            }
            _SaveFile_C_Disc[locPathFileNameNotAAA] = locMD5;
        }
    }
    if (locMD5 === md5) {
        if (!fileNameAAA) {
            if (!locFileBuffer) {
                locFileBuffer = _Fs.readFileSync(locPathFileNameNotAAA);
            }
            mu.returnMode = 1;
            mu.reverseOpts.notSendServerShowLog = false;
            mu.returnOpt.statusCode = 200;
            mu.returnOpt.headers = {
                'content-length': '' + locFileBuffer.length,
                'accept-ranges': 'bytes'
            };
            mu.returnOpt.decodedBody = locFileBuffer;
            console.log(`[${_TAG_}]本地包: ${locPathFileName}`.cyan);
            return;
        }
        else {
            const asset = GetAssetFileStartIndexSize(locPathFileName, locPathFileNameNotAAA);
            if (!asset) {
                console.log(`[${_TAG_}]本地文件存在，但没有asset配置！`.yellow, pathname);
                return;
            }
            const fileStartIndex = asset.startIndex;
            const fileSize = asset.size;
            if (locFileBuffer) {
                locFileBuffer = locFileBuffer.slice(fileStartIndex, fileStartIndex + fileSize);
                console.log(`[${_TAG_}]本地包(截取已读缓存):[${fileStartIndex}-${fileStartIndex + fileSize},${fileSize}]`.yellow, locPathFileName);
            }
            else {
                locFileBuffer = Buffer.alloc(fileSize);
                const fb = _Fs.openSync(locPathFileNameNotAAA, 'r', undefined);
                let bytesRead = _Fs.readSync(fb, locFileBuffer, 0, fileSize, fileStartIndex);
                _Fs.close(fb, (err) => {
                    if (err) {
                        console.log(`[${_TAG_}]文件读[close]:Error:`.red, locPathFileName, err);
                    }
                });
                if (bytesRead != fileSize) {
                    console.log(`[${_TAG_}]文件读取长度不一样：${fileSize} 读到：${bytesRead}`.magenta, locPathFileName);
                    return;
                }
            }
            mu.returnMode = 1;
            mu.reverseOpts.notSendServerShowLog = false;
            mu.returnOpt.statusCode = 200;
            mu.returnOpt.headers = {
                'content-length': '' + locFileBuffer.length,
                'accept-ranges': 'bytes'
            };
            mu.returnOpt.decodedBody = locFileBuffer;
            console.log(`[${_TAG_}]本地包[${fileStartIndex}-${fileStartIndex + fileSize},${fileSize}]:  ${locPathFileName}`.cyan);
            return;
        }
    }
    else {
        locFileBuffer = undefined;
        if (locMD5) {
            console.log(`[${_TAG_}]本地文件MD5不同: ${md5} , ${locMD5}`.gray);
        }
        else {
            _AppC.DetailLog && console.log(`[${_TAG_}]本地无文件`.gray);
        }
        return;
    }
}
exports.LocDownloadPackUrlHandle = LocDownloadPackUrlHandle;
function DownloadPackMerge(ufType, savePath) {
    const TAG = _TAG_ + '-数据包合并';
    if (ufType === 19)
        return;
    const nm = savePath.match(_Reg_DownloadPackNameAAA);
    const fileNameAAA = (nm && nm[1]) ? nm[1] : '';
    if (!fileNameAAA)
        return;
    const locPathFileNameNotAAA = fileNameAAA ? savePath.slice(0, -4) : savePath;
    const assetData = GetAssetData(savePath, locPathFileNameNotAAA);
    var isLocFileMD5OKAndFL = false;
    if (assetData) {
        if (_SaveFile_C_Disc[locPathFileNameNotAAA]) {
            if (_SaveFile_C_Disc[locPathFileNameNotAAA] === assetData.asset.md5) {
                return;
            }
            else if (_SaveFile_C_Disc[locPathFileNameNotAAA] === '[writeing]') {
                return;
            }
            else {
            }
        }
        else if (_Fs.existsSync(locPathFileNameNotAAA)) {
            let md5 = MD5ofBigFile(locPathFileNameNotAAA);
            if (assetData.asset.md5 === md5) {
                if (assetData.asset.file_list.length > 1) {
                    isLocFileMD5OKAndFL = true;
                }
                else {
                    return;
                }
            }
        }
        const asset = assetData.asset;
        const flLen = asset.file_list.length;
        const fList = [];
        for (let index = 0; index < flLen; index++) {
            const item = asset.file_list[index];
            const locPath = _Path.join(_FileBaseMagica, _DownloadPackUrl_Loc, item.url);
            let isHas = false;
            if (_SaveFile_C_Disc[locPath])
                isHas = true;
            else if (_Fs.existsSync(locPath))
                isHas = true;
            if (isHas) {
                fList.push({
                    locPath,
                    size: item.size,
                });
            }
            else {
                if (index + 1 === flLen) {
                    console.log(`[${TAG}]文件缺失：`.magenta, locPath);
                }
                return;
            }
        }
        try {
            if (!_SaveFile_C_Disc[locPathFileNameNotAAA])
                _SaveFile_C_Disc[locPathFileNameNotAAA] = '[writeing]';
            let ok = true;
            const md5Crypto = _Crypto.createHash('md5');
            if (isLocFileMD5OKAndFL == false) {
                const fd = _Fs.openSync(locPathFileNameNotAAA, 'w');
                let writePosition = 0;
                for (let index = 0; index < fList.length; index++) {
                    const item = fList[index];
                    let buffer = _Fs.readFileSync(item.locPath);
                    if (buffer.length != item.size) {
                        console.log(`[${TAG}]组合文件时异常： ${item.locPath}: Len: ${buffer.length} != ${item.size}`.magenta);
                        console.log();
                        ok = false;
                        break;
                    }
                    else {
                        _Fs.writeSync(fd, buffer, 0, buffer.length, writePosition);
                        writePosition += buffer.length;
                        md5Crypto.update(buffer);
                    }
                }
                _Fs.close(fd, (err) => {
                    err && console.warn(`[${TAG}]组合文件后，close失败`.red, err);
                });
            }
            if (ok === false) {
                console.log(`[组合文件失败]删除文件: ${locPathFileNameNotAAA}`.magenta);
                _Fs.unlinkSync(locPathFileNameNotAAA);
            }
            else {
                if (isLocFileMD5OKAndFL == false) {
                    let md5 = md5Crypto.digest('hex');
                    if (asset.md5 && md5 != asset.md5) {
                        console.log(`[${TAG}]组合文件失败，组合后MD5不一样！`.magenta, locPathFileNameNotAAA);
                    }
                    else {
                        _SaveFile_C_Disc[locPathFileNameNotAAA] = md5;
                        console.log(`[${TAG}]组合文件:`.blue, locPathFileNameNotAAA);
                        for (let index = 0; index < fList.length; index++) {
                            const item = fList[index];
                            console.log('删除文件:', item.locPath);
                            _Fs.unlinkSync(item.locPath);
                        }
                    }
                }
                else {
                    for (let index = 0; index < fList.length; index++) {
                        const item = fList[index];
                        console.log('删除文件:', item.locPath);
                        _Fs.unlinkSync(item.locPath);
                    }
                }
            }
        }
        catch (error) {
            console.log(`[${TAG}]组合文件时异常：`.magenta, fList, error);
        }
    }
    else {
        console.log(`[${TAG}]异常：无对应asset`.magenta, assetData);
        if (isDo_LoadLocAssetJson == false) {
            setTimeout(() => {
                LoadLocAssetJson();
            }, 0);
        }
    }
}
function ActDownloadPackMerge() {
    const TAG = _TAG_ + '-整合数据包';
    console.log();
    console.log(`[${TAG}]整合数据包开始。`.yellow);
    console.log();
    console.log(`[${TAG}]加载...`.green);
    LoadLocAssetJson();
    console.log();
    console.log(`[${TAG}]合并...`.green);
    for (const key in _Asset_C_Disc) {
        const asset = _Asset_C_Disc[key];
        const url = asset.file_list[0].url;
        const ufType = GetUrlFileType(_Path.posix.join(_DownloadPackUrl_Src, url));
        const savePath = _Path.posix.join(_FileBaseMagica, _DownloadPackUrl_Loc, url);
        DownloadPackMerge(ufType, savePath);
    }
    console.log(`[${TAG}]整合数据包结束。`.yellow);
    console.log();
}
_App.SetCmdsExt([
    {
        cmd: ['a1', 'DownloadPackMerge'],
        txt: '数据包整合。', func: () => {
            ActDownloadPackMerge();
        },
    },
    {
        cmd: ['a0', 'loadAsset'],
        txt: '加载Asset', func: () => {
            LoadLocAssetJson();
        },
    }
]);
const _304_C_Disc = {};
function StatusCode304Cache(urlPath, reqHeaders, resHeaders) {
    if (_AppC.Cache304ETag == false)
        return;
    if (urlPath && urlPath.lastIndexOf('.json.gz') === urlPath.length - 8) {
        return;
    }
    const reqETag = (reqHeaders && reqHeaders["if-none-match"]) || undefined;
    const resETag = (resHeaders && resHeaders["etag"]) || undefined;
    if (reqETag && reqETag == resETag && reqHeaders) {
        if (_304_C_Disc[urlPath] == undefined) {
            _304_C_Disc[urlPath] = {
                etag: reqETag,
                'last-modified': reqHeaders['last-modified'] || undefined,
            };
        }
        else {
            console.log('[已经有缓存]304的缓存的文件格式问题（非：png）：', urlPath.magenta);
        }
    }
    else {
        if (_304_C_Disc[urlPath])
            delete _304_C_Disc[urlPath];
    }
}
function StatusCode304Handle(urlPath, reqHeaders) {
    if (!(urlPath && urlPath.lastIndexOf('.png') === urlPath.length - 4)) {
        return false;
    }
    if (_304_C_Disc[urlPath]) {
        const reqETag = (reqHeaders && reqHeaders["if-none-match"]) || undefined;
        if (reqETag && _304_C_Disc[urlPath].etag == reqETag) {
            var rH = {
                etag: _304_C_Disc[urlPath].etag,
            };
            if (_304_C_Disc[urlPath]["last-modified"])
                rH['last-modified'] = _304_C_Disc[urlPath]["last-modified"];
            return rH;
        }
        else {
            delete _304_C_Disc[urlPath];
            return false;
        }
    }
    else {
        return false;
    }
}
exports.StatusCode304Handle = StatusCode304Handle;
function MD5(data) {
    const md5Crypto = _Crypto.createHash('md5');
    return md5Crypto.update(data).digest('hex');
}
function MD5ofFile(path) {
    const md5Crypto = _Crypto.createHash('md5');
    return md5Crypto.update(_Fs.readFileSync(path)).digest('hex');
}
function MD5ofBigFile(path) {
    try {
        const md5Crypto = _Crypto.createHash('md5');
        const fileSize = 2048;
        const fb = _Fs.openSync(path, 'r', undefined);
        let fileLen = 0;
        while (true) {
            let locFileBuffer = Buffer.alloc(fileSize);
            let rLen = _Fs.readSync(fb, locFileBuffer, 0, fileSize, fileLen);
            fileLen += rLen;
            if (rLen != fileSize) {
                _Fs.close(fb, (err) => { });
                md5Crypto.update(locFileBuffer.slice(0, rLen));
                return md5Crypto.digest('hex');
            }
            else {
                md5Crypto.update(locFileBuffer);
            }
        }
        _Fs.close(fb, (err) => { });
    }
    catch (error) {
        console.log(`[${_TAG_}]MD5大文件异常：`.red, error);
        return '[ERROR]';
    }
}
function CreateDirsSync(dir) {
    let paths = [];
    while (!_Fs.existsSync(dir)) {
        paths.push(dir);
        dir = _Path.dirname(dir);
    }
    for (let index = paths.length - 1; index >= 0; index--) {
        let dir = paths[index];
        _Fs.mkdirSync(dir);
    }
}
function CreateDirsOnFileSync(pathAndFile) {
    CreateDirsSync(_Path.dirname(pathAndFile));
}
function WriteFile(file, data, log) {
    try {
        _Fs.writeFile(file, data, (err) => {
            if (err)
                console.warn(`[写][${err.name}]:`.red, err.message);
            else if (log) {
                console.log(`[写]: ${file.replace(_FileBaseMagica, '[BaseMagica]')}`.blue);
            }
        });
    }
    catch (error) {
        console.warn('[写][Error]:'.red, error);
    }
}
function WriteFileSync(file, data, log) {
    try {
        _Fs.writeFileSync(file, data);
        log && console.log(`[写]: ${file.replace(_FileBaseMagica, '[BaseMagica]')}`.blue);
    }
    catch (error) {
        console.warn('[写][Error]:'.red, error);
    }
}
