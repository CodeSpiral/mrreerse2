"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
const _TAG_ = '数据收集配置';
const _FiltersComUnit = {
    gameUser: { inviteCode: '*' },
    userSectionList: [
        {
            section: ['-',
                'openEnemy',
                'openEnemyList',
                'openConditionSectionId',
                'openConditionSection',
                'openConditionCharaBondsPt',
                'openConditionMagiaLevel',
                'openConditionChapter',
                'openConditionRank',
                'message',
                'outline'
            ]
        },
    ],
    userQuestBattleList: [
        {
            questBattle: ['-',
                'bgm',
                'bossBgm'
            ]
        },
    ],
};
exports.JsonDoConf = [
    {
        name: '【掉落数据|关卡列表】',
        url: [{ path: '/magica/api/quest/native/result/send' }],
        filter: {
            gameUser: _FiltersComUnit.gameUser,
            userQuestBattleResultList: [
                {
                    questBattleId: '*',
                    battleType: '*',
                    questBattleStatus: '*',
                    dropRewardCodes: '*',
                }
            ],
            userSectionList: _FiltersComUnit.userSectionList,
            userQuestBattleList: _FiltersComUnit.userQuestBattleList,
        },
        CanSendAsJson(json) {
            if (json) {
                if (json.userQuestBattleResultList && json.userQuestBattleResultList instanceof Array) {
                    if (json.userQuestBattleResultList[0]) {
                        const data = json.userQuestBattleResultList[0];
                        if (!data.questBattleStatus) {
                            return false;
                        }
                        if (data.questBattleStatus !== 'SUCCESSFUL') {
                            console.log(`[${_TAG_}][${this.name}]不发送: questBattleStatus: ${data.questBattleStatus}`.gray);
                            return false;
                        }
                        if (!data.battleType || data.battleType == 'ARENA') {
                            console.log(`[${_TAG_}][${this.name}]不发送: battleType: ${data.battleType}`.gray);
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                }
            }
            return false;
        },
    },
    {
        name: '【关卡列表】',
        url: [
            { path: '/magica/api/page/', query: 'userQuestBattleList', },
            { path: '/magica/api/page/EventBranchTop', },
        ],
        filter: {
            userSectionList: _FiltersComUnit.userSectionList,
            userQuestBattleList: _FiltersComUnit.userQuestBattleList,
            userEventBranchPointList: [
                {
                    "point": {
                        "eventId": '*',
                        "questBattleId": '*',
                        "title": '*',
                    },
                }
            ],
        }
    },
    {
        name: '【战斗数据】',
        url: [{ path: '/magica/api/quest/native/get' }],
        filter: {
            battleType: '*',
            scenario: '*',
            waveList: '*',
            artList: '*',
            memoriaList: '*',
            magiaList: '*',
            connectList: '*',
            doppelList: '*',
            webData: {
                resultCode: '*',
                gameUser: _FiltersComUnit.gameUser,
                userQuestBattleResultList: [
                    { questBattleId: '*', }
                ]
            }
        },
        CanSendAsJson(json) {
            if (config_1.Conf.CollectionQuestBattleGet == false)
                return false;
            if (json) {
                const bType = json.battleType;
                if (!bType || bType == 'ARENA') {
                    return false;
                }
                return true;
            }
            else {
                return false;
            }
        },
    },
    {
        name: '【抽卡】',
        url: [{ path: '/magica/api/gacha/draw' }],
        filter: {
            resultCode: "*",
            gameUser: _FiltersComUnit.gameUser,
            gachaAnimation: '*',
            userPieceList: [
                { piece: '*' }
            ],
            userCharaList: [
                { chara: '*' }
            ],
            userCardList: [
                { card: '*' }
            ],
        }
    },
    {
        name: '【记忆列表&素材】',
        url: [{ path: '/magica/api/page/TopPage', query: 'pieceList' }],
        filter: {
            pieceList: '*',
            giftList: '*',
        },
    },
    {
        name: '【MyPage角色列表】',
        url: [
            { path: '/magica/api/page/MyPage', query: 'userCharaList', },
            { path: '/magica/api/page/MyPage', query: 'userCardList', },
            { path: '/magica/api/page/MyPage', query: 'userPieceList', },
        ],
        filter: {
            userPieceList: [
                { piece: '*' }
            ],
            userCharaList: [
                { chara: '*' }
            ],
            userCardList: [
                { card: '*' }
            ],
        }
    },
    {
        name: '【图鉴角色列表|关卡列表】',
        url: [{ path: '/magica/api/page/CharaCollection' }],
        filter: {
            "charaList": [
                {
                    "chara": '*',
                    "cardList": [
                        { "card": '*', }
                    ]
                }
            ],
            userSectionList: _FiltersComUnit.userSectionList,
        }
    },
    {
        name: '【图鉴记忆列表】',
        url: [{ path: '/magica/api/page/PieceCollection' }],
        filter: {
            userPieceCollectionList: [
                { piece: '*' }
            ]
        }
    },
    {
        name: '【支援列表】',
        url: [{ path: '/magica/api/page/SupportSelect' }],
        filter: {
            supportUserList: [
                {
                    userCardList: [
                        { card: '*' }
                    ],
                    userCharaList: [
                        { chara: '*' }
                    ],
                    userPieceList: [
                        { piece: '*' }
                    ],
                    inviteCode: "*",
                }
            ]
        },
        CanSendAsJson(json) {
            return (!!json.supportUserList);
        }
    },
    {
        name: '【商店】',
        url: [{ path: '/magica/api/page/ShopTop' }],
        filter: {
            shopList: [{
                    shopItemList: [{
                            card: '*',
                            chara: '*',
                            piece: '*',
                        }]
                }]
        },
    },
];
const _pyDev_ = false;
function Check_pySendRule(url) {
    if (_pyDev_ == false)
        return;
    if (url) {
        for (let index = 0; index < pySendRule_RE.length; index++) {
            const re = pySendRule_RE[index];
            if (re.test(url)) {
                console.log(`[RegExp]: ${url}`.magenta);
                return;
            }
        }
    }
}
exports.Check_pySendRule = Check_pySendRule;
const pySendRule = {
    'https://(ios|android).magi-reco.com/magica/api/quest/native/result/send': {
        'gameUser': { 'inviteCode': '*' },
        'userQuestBattleResultList': [
            {
                'questBattleId': '*',
                'dropRewardCodes': '*',
            }
        ],
        'userSectionList': [
            { 'section': '*' }
        ],
        'userQuestBattleList': [
            { 'questBattle': '*' }
        ],
    },
    'https://(ios|android).magi-reco.com/magica/api/quest/native/get': {
        'scenario': '*',
        'waveList': '*',
        'artList': '*',
        'memoriaList': '*',
        'magiaList': '*',
        'connectList': '*',
        'doppelList': '*',
        'webData': {
            'gameUser': { 'inviteCode': '*' },
            'userQuestBattleResultList': [
                { 'questBattleId': '*', }
            ]
        }
    },
    'https://(ios|android).magi-reco.com/magica/api/page/TopPage\?value=[\w\d,]*pieceList[\w\d,]*&timeStamp=[\d]+': {
        'pieceList': '*',
        'giftList': '*',
    },
    'https://(ios|android).magi-reco.com/magica/api/page/MyPage\?value=[\w\d,]*(userCharaList|userCardList|userPieceList)[\w\d,]*&timeStamp=[\d]+': {
        'userPieceList': [
            { 'piece': '*' }
        ],
        'userCharaList': [
            { 'chara': '*' }
        ],
        'userCardList': [
            { 'card': '*' }
        ],
    },
    'https://(ios|android).magi-reco.com/magica/api/page/CharaCollection\?(value=[\w\d,]*&)?timeStamp=[\d]+': {
        "charaList": [
            {
                "chara": '*',
                "cardList": [
                    { "card": '*', }
                ]
            }
        ],
        'userSectionList': [
            { 'section': '*' }
        ],
    },
    'https://(ios|android).magi-reco.com/magica/api/page/[\w\d/]+\?value=[\w\d,]*(userSectionList|userQuestBattleList)[\w,]*&timeStamp=[\d]+': {
        'userSectionList': [
            { 'section': '*' }
        ],
        'userQuestBattleList': [
            { 'questBattle': '*' }
        ],
        'userEventBranchPointList': [
            {
                "point": {
                    "eventId": '*',
                    "questBattleId": '*',
                    "title": '*',
                },
            }
        ],
    },
    'https://(ios|android).magi-reco.com/magica/api/page/EventBranchTop\?value=[\w\d,]*&timeStamp=[\d]+': {
        'userSectionList': [
            { 'section': '*' }
        ],
        'userQuestBattleList': [
            { 'questBattle': '*' }
        ],
        'userEventBranchPointList': [
            {
                "point": {
                    "eventId": '*',
                    "questBattleId": '*',
                    "title": '*',
                },
            }
        ],
    },
    'https://(ios|android).magi-reco.com/magica/api/gacha/draw': {
        'resultCode': "*",
        'gameUser': { 'inviteCode': '*' },
        'gachaAnimation': '*',
        'userPieceList': [
            { 'piece': '*' }
        ],
        'userCharaList': [
            { 'chara': '*' }
        ],
        'userCardList': [
            { 'card': '*' }
        ],
    },
    'https://(ios|android).magi-reco.com/magica/api/page/PieceCollection\?timeStamp=[\d]+': {
        'userPieceCollectionList': [
            { 'piece': '*' }
        ]
    },
    'https://(ios|android).magi-reco.com/magica/api/page/SupportSelect': {
        'supportUserList': [
            {
                'userCardList': [
                    { 'card': '*' }
                ],
                'userCharaList': [
                    { 'chara': '*' }
                ],
                'userPieceList': [
                    { 'piece': '*' }
                ],
                'inviteCode': "*",
            }
        ]
    },
    'https://(ios|android).magi-reco.com/magica/api/page/ShopTop\?value=[\w\d,]*&timeStamp=[\d]+': {
        'shopList': [
            {
                'shopItemList': [
                    {
                        'card': '*',
                        'chara': '*',
                        'piece': '*',
                    }
                ]
            }
        ]
    }
};
const pySendRule_RE_defTestUrl = [
    '/magica/api/page/TopPage?value=user,gameUser,itemList,giftList,pieceList,userQuestAdventureList&timeStamp=1545134318026',
    '/magica/api/page/MyPage?value=user,gameUser,userStatusList,userCharaList,userCardList,userDoppelList,userItemList,userGiftList,userDailyChallengeList,userTotalChallengeList,userLimitedChallengeList,userGiftList,userPieceList,userPieceSetList,userDeckList,userLive2dList&timeStamp=1545136115222',
    '/magica/api/page/PieceCollection?timeStamp=1545136387720',
    '/magica/api/page/CharaCollection?value=userSectionList&timeStamp=1545136559358',
    '/magica/api/page/CharaCollection?timeStamp=1545137747416',
    '/magica/api/page/EventDailyTowerTop?value=userChapterList,userQuestBattleList,userFollowList&timeStamp=1545136673653',
    '/magica/api/page/ShopTop?value=userFormationSheetList&timeStamp=1545136589210',
    '/magica/api/page/SupportSelect',
    '/magica/api/quest/native/get',
    '/magica/api/quest/native/result/send',
    '/magica/api/gacha/draw',
];
const pySendRule_RE = (() => {
    if (_pyDev_ == false)
        return [];
    let l = [];
    for (const key in pySendRule) {
        let re = '^' + key.replace('https://(ios|android).magi-reco.com', '')
            .replace('[wd/]', '[\\w\\d/]')
            .replace(/\[wd,\]/g, '[\\w\\d,]')
            .replace(/\[w,\]/g, '[\\w,]')
            .replace('[d]', '[\\d]')
            .replace('?', '\\?')
            + '$';
        l.push(new RegExp(re));
    }
    console.log(l);
    console.log();
    for (let index = 0; index < pySendRule_RE_defTestUrl.length; index++) {
        const url = pySendRule_RE_defTestUrl[index];
        console.log('  ' + url.gray);
        for (let index = 0; index < l.length; index++) {
            const re = l[index];
            if (re.test(url)) {
                console.log(`   -> [${index}][RegExp]: ${re}`.magenta);
                break;
            }
        }
        console.log();
    }
    console.log();
    return l;
})();
