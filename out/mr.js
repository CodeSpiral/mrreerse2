"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _App = require("./config");
const _AppConf = _App.Conf;
const _MrHandle = require("./mrHandle");
exports.asyncMrHandle = _MrHandle.AsyncMrHandle;
const _TAG_ = 'MR';
function CreateMrUnit(ru, urlPathSrc, urlPathParse, geterClientReq, reqMethod) {
    let urlPath = urlPathParse.pathname || '';
    let revIsNewConnect = false;
    let revClearReqUnitDataSet = false;
    if (urlPath == '/magica/index.html') {
        revClearReqUnitDataSet = true;
    }
    const reqHeaders = geterClientReq.GetClientReqHeaders();
    let mu = {
        tag: ru.tag,
        lastErrorMsg: '',
        reqMethod: reqMethod,
        reverseOpts: {
            reques: {
                headers: false,
                decodedBody: false,
            },
            notSendServerShowLog: true,
            showLogClientReqBody: false,
            revIsNewConnect: revIsNewConnect,
            revClearReqUnitDataSet: revClearReqUnitDataSet,
        },
        handleConf: {
            aborteAction: false,
            doReqHeaders: [],
            doReqBody: [],
            doResponseBody: [],
            doResponseHeaders: [],
        },
        returnMode: 0,
        returnOpt: {},
        _req: {
            urlPathSrc,
            urlPath,
            urlPathParse,
            headers: reqHeaders,
            GetDecodedBody() { return geterClientReq.GetClientReqDecodedBody(); },
        },
        _res: {}
    };
    if (_AppConf.Cache304ETag) {
        const s304Headers = _MrHandle.StatusCode304Handle(urlPath, reqHeaders);
        if (s304Headers) {
            console.log(`[${_TAG_} ${ru.tag}]${urlPath} -> 304`.gray);
            SetReturnMuNotSendServer(mu, 304, s304Headers, '');
            mu.reverseOpts.notSendServerShowLog = false;
            return mu;
        }
    }
    if (urlPath === '/magica/api/test/logger/error') {
        SetReturnMuNotSendServer(mu, 200, {
            'content-type': 'application/json;charset=UTF-8',
            'pragma': 'no-cache',
            'cache-control': 'no-cache',
        }, JSON.stringify({
            resultCode: 'error',
            title: '请求被拦截',
            errorTxt: '拦截请求:错误信息',
        }));
        console.log();
        console.log(`[${_TAG_}]拦截请求:错误信息`.magenta);
        const body = geterClientReq.GetClientReqDecodedBody();
        console.log(body ? body.toString() : 'body没有内容。');
        console.log();
        return mu;
    }
    if (urlPath === '/favicon.ico') {
        SetReturnMuNotSendServer(mu, 404, {}, '');
        console.log(`[${_TAG_}]直接返回:/favicon.ico -> 404`.blue);
        return mu;
    }
    if ((urlPath.indexOf('/Ban') >= 0 && urlPath.indexOf('/BannerView.js') === -1)
        || (urlPath.indexOf('/magica/api/page/Backdoor') >= 0)) {
        SetReturnMuNotSendServer(mu, 500, {
            'content-type': 'application/json;charset=UTF-8',
            'pragma': 'no-cache',
            'cache-control': 'no-cache',
        }, '');
        console.log();
        console.log(` [${_TAG_}]BAN ！？ 拦截！并终止后续处理 `.red.bgYellow, urlPath.yellow);
        const body = geterClientReq.GetClientReqDecodedBody();
        console.log(body ? body.toString() : 'body没有内容。');
        console.log();
        return mu;
    }
    if (ru.clientReq.hostname && ru.clientReq.hostname.indexOf('.magi-reco.com') === -1) {
        if (ru.clientReq.hostname.indexOf('127.0.0.1') === -1) {
            console.log(` [${_TAG_}]域名: ${ru.clientReq.hostname} 不处理。 `.red);
            return mu;
        }
    }
    if (_AppConf.UseLocDownloadPack) {
        _MrHandle.LocDownloadPackUrlHandle(mu);
    }
    if (_AppConf.LocDownloadPackServerMode) {
        if (urlPath.indexOf('/magica/api/') >= 0
            && urlPath.indexOf('/magica/api/page/TopPage') == -1) {
            return SetReturnMuNotSendServer(mu, 500, {}, '');
        }
    }
    else if (_ExtCreateMrUnit) {
        _ExtCreateMrUnit(mu);
    }
    return mu;
}
exports.CreateMrUnit = CreateMrUnit;
let _ExtCreateMrUnit;
function SetReturnMuNotSendServer(mu, statusCode, header, body) {
    mu.returnMode = 1;
    mu.returnOpt.headers = header || {
        'pragma': 'no-cache',
        'cache-control': 'no-cache',
    };
    mu.returnOpt.decodedBody = (body === '' ? Buffer.alloc(0) : Buffer.from(body));
    mu.returnOpt.statusCode = statusCode || 500;
    return mu;
}
function SetExtCreateMrUnit(func) {
    _ExtCreateMrUnit = func;
}
exports.SetExtCreateMrUnit = SetExtCreateMrUnit;
setTimeout(() => {
    if (_App._IsDev) {
        try {
            require('./mrExt/mrExt');
        }
        catch (error) {
            console.error('[MR-Ex]', error);
        }
    }
}, 0);
