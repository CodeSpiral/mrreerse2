"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Http = require("http");
const _Zlib = require("zlib");
const config_1 = require("./config");
const sendCollectionC_1 = require("./sendCollectionC");
const _IS_DEV_ = !true;
if (_IS_DEV_)
    console.log(` [注意] ${__filename} DEV 中... `.red);
const _ServerIp = config_1.Conf.CollectionServerIp;
const _ServerPort = config_1.Conf.CollectionServerPort;
function CheckCanSend() {
    return config_1.Conf.SendToCollectionServer && !!_ServerIp && !!_ServerPort;
}
const _TAG_ = '收集数据';
function Send(urlPath, body) {
    if (CheckCanSend()) {
        if (!urlPath) {
            console.log('发送到收集服务器的urlPath没有数据！');
        }
        try {
            if (_IS_DEV_)
                return;
            let options = {
                hostname: _ServerIp,
                port: _ServerPort,
                path: urlPath,
                method: 'POST',
                headers: {
                    'content-encoding': 'gzip',
                }
            };
            let buf = _Zlib.gzipSync(body);
            let req = _Http.request(options, function (res) { res.destroy(); }).on('error', (e) => {
                console.error(`[${_TAG_}]请求遇到问题: ${e.message}`.red);
            });
            req.end(buf, function () {
            });
        }
        catch (err) { }
    }
}
function Send_HI() {
    if (_IS_DEV_)
        return;
    if (CheckCanSend()) {
        try {
            let options = {
                hostname: _ServerIp,
                port: _ServerPort,
                path: '/hi',
                method: 'POST',
            };
            let req = _Http.request(options, function (res) {
                let dataBuf;
                let dataStr;
                res.on('data', function (chunk) {
                    if (typeof chunk === 'string')
                        dataStr = dataStr ? (dataStr + chunk) : chunk;
                    else
                        dataBuf = Buffer.concat([(dataBuf ? dataBuf : Buffer.alloc(0)), chunk]);
                }).on('end', function () {
                    if (dataBuf)
                        dataStr = dataBuf.toString();
                    console.log(`[收集服务器HI][${_ServerIp}:${_ServerPort}]响应:`.yellow, dataStr);
                }).on('error', (e) => {
                    console.error(`[收集服务器HI][${_ServerIp}:${_ServerPort}]响应ERROR:`.red, e);
                });
            }).on('error', (e) => {
                console.error(`[收集服务器HI][${_ServerIp}:${_ServerPort}]ERROR:`.red, e);
            });
            req.end(function () { });
        }
        catch (err) { }
    }
}
if (CheckCanSend()) {
    setTimeout(() => {
        Send_HI();
    }, 100);
}
function CheckConfList() {
    const uL = {};
    for (let index = 0; index < sendCollectionC_1.JsonDoConf.length; index++) {
        const conf = sendCollectionC_1.JsonDoConf[index];
        for (let iU = 0; iU < conf.url.length; iU++) {
            const uu = conf.url[iU];
            const path = uu.path + (uu.query ? (' + ' + uu.query) : '');
            if (uL[path]) {
                console.warn(`[${_TAG_}]重复配置path: [${conf.name}] 与 [${uL[path]}]：`.red, uu);
            }
            uL[path] = conf.name;
        }
    }
}
CheckConfList();
function CheckSend_Something(urlStr, urlParse, body) {
    if (CheckCanSend() == false)
        return;
    const urlPath = urlParse.path || '';
    const urlQuery = urlParse.query || '';
    if (!urlPath)
        return;
    let json = undefined;
    for (let iCL = 0; iCL < sendCollectionC_1.JsonDoConf.length; iCL++) {
        const doConf = sendCollectionC_1.JsonDoConf[iCL];
        let canDo = false;
        for (let index = 0; index < doConf.url.length; index++) {
            const uu = doConf.url[index];
            if (urlPath.indexOf(uu.path) === 0) {
                if (uu.query) {
                    if (urlQuery.indexOf(uu.query) > 0) {
                        canDo = true;
                        break;
                    }
                }
                else {
                    canDo = true;
                    break;
                }
            }
        }
        if (canDo) {
            if (typeof json !== 'object') {
                let r = JsonParse(body.toString());
                if (r && typeof r === 'object') {
                    json = r;
                }
                else {
                    console.log(`[${_TAG_}][${doConf.name}]body的JSON转换失败！ 终止这次的处理`.red, doConf);
                    return;
                }
            }
            if (doConf.CanSendAsJson) {
                if (doConf.CanSendAsJson(json) == false) {
                    return;
                }
            }
            const sJ = JSON.stringify(lib_filterObject(doConf.filter, json));
            Send(urlStr, sJ);
            return;
        }
    }
}
exports.CheckSend_Something = CheckSend_Something;
function JsonParse(str) {
    if (str) {
        try {
            return JSON.parse(str);
        }
        catch (err) {
            console.log(`json转换失败！`.red, err);
            return false;
        }
    }
    return undefined;
}
exports.JsonParse = JsonParse;
function lib_filterObject(filter, obj) {
    if (filter instanceof Array) {
        if (filter.length == 1 && obj instanceof Array) {
            let res = [];
            for (let index = 0; index < obj.length; index++) {
                res.push(lib_filterObject(filter[0], obj[index]));
            }
            return res;
        }
        else if (typeof obj === 'object') {
            if (filter[0] === '-') {
                for (let index = 0; index < filter.length; index++) {
                    delete obj[filter[index]];
                }
                return obj;
            }
            else {
                return `[ERROR-A(${filter[0]})->O]`;
            }
        }
        else {
            return `[ERROR-A]`;
        }
    }
    else if (typeof filter === 'object' && typeof obj === 'object') {
        let res = {};
        for (const key in filter) {
            if (!filter.hasOwnProperty(key))
                continue;
            if (!obj.hasOwnProperty(key))
                continue;
            if (obj[key] === undefined)
                continue;
            res[key] = (filter[key] == '*') ? obj[key] : lib_filterObject(filter[key], obj[key]);
        }
        return res;
    }
    else {
        return obj;
    }
}
