"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Url = require("url");
const _Http2 = require("http2");
const { HTTP2_HEADER_METHOD, HTTP2_HEADER_PATH, HTTP2_HEADER_STATUS, HTTP2_HEADER_CONTENT_TYPE, HTTP2_HEADER_AUTHORITY, } = _Http2.constants;
const _Colors = require("colors");
const _Zlib = require("zlib");
const _ReqUnit = require("../lib/httpReqUnit");
const _Req = require("../lib/http2Req");
const _DEBUG_LOG_ = false;
function CreateHttp2ReverseServer(domainConf, options) {
    const TAG = 'H2RS';
    if (!options.cert)
        return 'cert is null!';
    if (!options.key)
        return 'cert key is null!';
    const listenPort = options.listenPort;
    const isLog = options.isLog === true;
    const isReerseMultiUser = options.isReerseMultiUser ? true : false;
    const tunnel = options.tunnel;
    let unit;
    let ssOpts = {
        cert: options.cert,
        key: options.key,
        allowHTTP1: true,
    };
    const server = _Http2.createSecureServer(ssOpts);
    server.on('error', (err) => {
        console.error(`[${TAG}][Error]`.red, err);
    });
    unit = {
        OnRequest() { console.error(`[${TAG}][Error]onRequestHeader null 未初始化`.red); return undefined; },
        Start() {
            server.listen(listenPort, (err) => {
                if (err) {
                    console.error(`[${TAG}][Error]`.red, err);
                    return process.exit(1);
                }
                else {
                    console.log(`监听 https://127.0.0.1:${listenPort}`.bgYellow.magenta);
                }
            });
        }
    };
    if (domainConf) {
        for (let index = 0; index < domainConf.length; index++) {
            const item = domainConf[index];
            console.log('附加域名: '.yellow, item.domain);
            server.addContext(item.domain, {
                cert: item.cert,
                key: item.key,
            });
        }
    }
    server.on('request', newServerOnRequest(unit, isLog, isReerseMultiUser, tunnel));
    server.on('tlsClientError', (err, tlsSocket) => {
        console.log('[server-_Tls.Server]tlsClientError: INFO:'.magenta);
        let certificate = tlsSocket.getPeerCertificate();
        console.log('[Certificate]', certificate);
        console.log('[server-_Tls.Server]tlsClientError.'.red, err);
    });
    return unit;
}
exports.CreateHttp2ReverseServer = CreateHttp2ReverseServer;
function newServerOnRequest(unit, isLog, isReerseMultiUser, optsTunnel) {
    let serverCountNum = 0;
    return (req, response) => {
        const nowN = (++serverCountNum);
        if (serverCountNum >= 99999) {
            serverCountNum = 0;
        }
        const isHttps2 = (req.httpVersion === '2.0');
        const headers = req.headers;
        const method = req.method || '';
        const path = (isHttps2 ? headers[HTTP2_HEADER_PATH] : req.url) || '';
        const webHost = (isHttps2 ? headers[HTTP2_HEADER_AUTHORITY] : headers['host']) || '';
        const authority = 'https://' + webHost;
        const nowTag = `${nowN}-` + (isHttps2 ? 'H2' : 'H1');
        if (isLog)
            console.log(`-> [${nowTag}][`.green + (method == 'GET' ? method.green : method.yellow) + `]${path}`.green);
        const ruClientReq = {
            pathSrc: path,
            pathParse: _Url.parse(path),
            hostname: webHost,
            method: method,
            headers: headers,
            bodySrc: undefined,
            isEnd: false,
            isClose: false,
        };
        const ruReverseResponse = {
            statusCode: undefined,
            headers: undefined,
            bodySrc: undefined,
        };
        const ru = {
            isHttps2,
            indexId: nowN,
            tag: nowTag,
            startTime: Date.now(),
            clientReq: ruClientReq,
            reverseReq: undefined,
            reverseResponse: ruReverseResponse,
            clientResponse: undefined,
        };
        let dataBuf = Buffer.alloc(0);
        req.on('data', (chunk) => {
            dataBuf = Buffer.concat([dataBuf, chunk], (dataBuf.length + chunk.length));
        });
        req.on('end', () => {
            ruClientReq.isEnd = true;
            ruClientReq.bodySrc = dataBuf;
            OnRequest(ru);
        });
        req.on('close', () => {
            ruClientReq.isClose = true;
            _DEBUG_LOG_ && console.log(`[${nowTag}][req-close]`.red);
        });
        req.on('err', (err) => {
            console.error(`[${nowTag}][req-error]`.red, err);
            if (ruClientReq.isEnd == false)
                OnRequest(ru);
        });
        let isDo_OnRequest = false;
        function OnRequest(ru) {
            if (isDo_OnRequest)
                return;
            isDo_OnRequest = true;
            const crOptsSS = {
                isResponseHeaders: false,
                isOnResDataSend: false,
                changeHeaders: false,
                changeBody: false,
            };
            const crOpts = {
                doReverse: true,
                flagReverseSocketCloseLog: !isReerseMultiUser,
                reverseReq: {
                    authority: authority,
                    headers: ruClientReq.headers,
                    bodySrc: ruClientReq.bodySrc,
                    canNewConnect: isReerseMultiUser,
                    clearReqUnitDataSet: false,
                },
                clientResponse: {
                    statusCode: undefined,
                    headers: undefined,
                    bodySrc: undefined,
                    responseBodyNullWarnLog: true,
                },
                isWaitEndResponseHeaders: false,
                isWaitEndResponseBody: false,
                GetClientReqDecodedBody() {
                    const gzip = ruClientReq.headers['content-encoding'];
                    if (gzip && ruClientReq.bodySrc)
                        return _ReqUnit.ZlibGunzipSync(gzip, ruClientReq.bodySrc);
                    else
                        return ruClientReq.bodySrc;
                },
            };
            ru.reverseReq = crOpts.reverseReq;
            ru.clientResponse = crOpts.clientResponse;
            unit.OnRequest(ru, crOpts);
            if (crOpts.reverseReq.clearReqUnitDataSet) {
                _Req.DeleteClientUnitDataSet(crOpts.reverseReq.authority);
            }
            const stream = req.stream;
            if (crOpts.doReverse) {
                _Req.Request({
                    isIndependent: crOpts.reverseReq.canNewConnect,
                    tunnel: {
                        use: optsTunnel.use(),
                        type: 'socket5',
                        ip: '127.0.0.1',
                        port: optsTunnel.tunnelPort,
                    },
                    httpVersion: isHttps2 ? '2.0' : '1.1',
                    authority: crOpts.reverseReq.authority,
                    method: method,
                    path: ruClientReq.pathSrc,
                    headers: crOpts.reverseReq.headers,
                    body: crOpts.reverseReq.bodySrc || (method == 'POST' ? '' : undefined),
                    bodyAsyncSend: false,
                    OnError(u, tag, error) {
                        console.error(`[${ru.tag}|${u.req.tag}][Error:${_ReqUnit.RequestError[tag]}]OnError:`.red, error.message);
                    },
                    OnRequestErrorAndEnd(u, tag, error) {
                        console.error(`[${ru.tag}|${u.req.tag}][Error:${_ReqUnit.RequestError[tag]}]OnRequestErrorAndEnd:`.red, error.message);
                    },
                    OnResHeaders(u, resHeaders) {
                        const srcStatusCode = u.res.statusCode;
                        ruReverseResponse.headers = resHeaders;
                        ruReverseResponse.statusCode = srcStatusCode;
                        const s_headers = {};
                        const s_statusCode = srcStatusCode;
                        for (const key in resHeaders) {
                            const element = resHeaders[key];
                            s_headers[key] = element;
                        }
                        if (crOpts.OnResponseHeader)
                            crOpts.OnResponseHeader(s_statusCode, s_headers);
                        if (undefined == crOpts.clientResponse.statusCode) {
                            crOpts.clientResponse.statusCode = srcStatusCode;
                        }
                        else {
                            crOptsSS.changeHeaders = true;
                        }
                        if (undefined == crOpts.clientResponse.headers) {
                            crOpts.clientResponse.headers = {};
                            for (const key in resHeaders)
                                crOpts.clientResponse.headers[key] = resHeaders[key];
                        }
                        else {
                            crOptsSS.changeHeaders = true;
                        }
                        if (crOpts.isWaitEndResponseBody && crOpts.clientResponse.headers['content-length'] !== undefined) {
                            crOpts.isWaitEndResponseHeaders = true;
                        }
                        if (crOpts.isWaitEndResponseHeaders) {
                            crOpts.isWaitEndResponseBody = true;
                        }
                        else {
                            if (crOptsSS.isResponseHeaders == false) {
                                crOptsSS.isResponseHeaders = true;
                                ReverseResponseHeaders({
                                    tag: `${ru.tag}|${u.req.tag}`,
                                    responseHeaders: crOpts.clientResponse.headers,
                                    statusCode: crOpts.clientResponse.statusCode,
                                    isHttps2,
                                    response,
                                    stream,
                                });
                            }
                            else {
                                console.warn(`[${ru.tag}|${u.req.tag}][WARN]:Headers已经响应给客户端！`.red);
                            }
                        }
                    },
                    OnResData(u, chunk, data) {
                        if (crOpts.isWaitEndResponseBody)
                            return;
                        crOptsSS.isOnResDataSend = true;
                        if (isHttps2) {
                            if (stream.writable)
                                stream.write(chunk);
                        }
                        else {
                            if (response.socket.writable)
                                response.write(chunk);
                        }
                    },
                    OnResEnd(u, res, body) {
                        ruReverseResponse.bodySrc = body.length ? body : undefined;
                        if (crOpts.OnResponseBody)
                            crOpts.OnResponseBody(crOpts, res.GetDecodedBuffer());
                        const isWaitEnd = crOpts.isWaitEndResponseBody;
                        if (isWaitEnd) {
                            let rBody = crOpts.clientResponse.bodySrc;
                            if (rBody) {
                                crOptsSS.changeBody = true;
                                const gzip = crOpts.clientResponse.headers['content-encoding'] || '';
                                switch (gzip) {
                                    case '': break;
                                    case 'gzip':
                                        rBody = _Zlib.gzipSync(rBody);
                                        break;
                                    default:
                                        console.error(`[${ru.tag}|${u.req.tag}][Error]OnResEnd(): 不明的压缩类型：`.red, gzip);
                                        break;
                                }
                            }
                            else {
                                if (crOpts.clientResponse.responseBodyNullWarnLog)
                                    console.warn(`[${ru.tag}|${u.req.tag}][WARN]OnResEnd(): isWaitEnd:${isWaitEnd}, rBody没有数据`.red);
                                rBody = body;
                            }
                            crOpts.clientResponse.bodySrc = rBody;
                            if (crOpts.isWaitEndResponseHeaders) {
                                if (crOptsSS.isResponseHeaders == false) {
                                    crOptsSS.isResponseHeaders = true;
                                    const responseHeaders = crOpts.clientResponse.headers;
                                    if (responseHeaders['content-length'] !== undefined) {
                                        responseHeaders['content-length'] = '' + rBody.byteLength;
                                    }
                                    console.log(`[${ru.tag}|${u.req.tag}][OnResEnd]回传headers.`.magenta);
                                    const rhr = ReverseResponseHeaders({
                                        tag: `${ru.tag}|${u.req.tag}`,
                                        responseHeaders: responseHeaders,
                                        statusCode: crOpts.clientResponse.statusCode,
                                        isHttps2,
                                        response,
                                        stream,
                                    });
                                    if (rhr === false) {
                                        if (u.res.statusCode === 200 && body.length == 0) {
                                            console.warn(`[${ru.tag}|${u.req.tag}][WARN]:Headers响应失败，并且源body没有数据！`.red);
                                        }
                                    }
                                }
                                else {
                                    console.warn(`[${ru.tag}|${u.req.tag}][WARN]:Headers已经响应给客户端！`.red);
                                }
                            }
                            if (isHttps2) {
                                if (stream.writable)
                                    stream.end(rBody);
                                else
                                    console.error(`[${ru.tag}|${u.req.tag}][Error](1)OnResEnd(): isWaitEnd:${isWaitEnd}, 响应socket不可写`.red);
                            }
                            else {
                                if (response.socket.writable)
                                    response.end(rBody);
                                else
                                    console.error(`[${ru.tag}|${u.req.tag}][Error](2)OnResEnd(): isWaitEnd:${isWaitEnd}, 响应socket不可写`.red);
                            }
                        }
                        else {
                            if (isHttps2) {
                                if (stream.writable)
                                    stream.end();
                            }
                            else {
                                if (response.socket.writable)
                                    response.end();
                            }
                        }
                        if (!crOptsSS.isResponseHeaders) {
                            console.error(`[${ru.tag}|${u.req.tag}][WARN]2[OnResEnd]: 还没有响应header过就返回了body！`.red);
                        }
                        let statusCode = ('' + u.res.statusCode) || '?';
                        if (statusCode == '200')
                            statusCode = _Colors.green(statusCode);
                        else if (statusCode == '304') { }
                        else
                            statusCode = _Colors.red(statusCode);
                        let bodyLen = body.byteLength;
                        if (bodyLen == 0)
                            bodyLen = '0b'.gray;
                        else if (bodyLen >= (1024 * 1024))
                            bodyLen = ((bodyLen / (1024 * 1024)).toFixed(3) + 'Mib').yellow;
                        else if (bodyLen >= 1024)
                            bodyLen = (bodyLen / 1024).toFixed(3) + 'Kib';
                        else
                            bodyLen = bodyLen + 'b';
                        let time = (Date.now() - ru.startTime) / 1000;
                        let timeTxt = `${time}s`;
                        if (time > 9.9)
                            timeTxt = timeTxt.red;
                        else if (time > 4)
                            timeTxt = timeTxt.yellow;
                        else if (time > 1)
                            timeTxt = timeTxt;
                        else
                            timeTxt = timeTxt.green;
                        const cH = crOptsSS.changeHeaders ? 'H' : ' ';
                        const cB = crOptsSS.changeBody ? 'B' : ' ';
                        console.log(`-> [${ru.tag}|${u.req.tag}][${u.res.localPort}->${u.res.remotePort}]`.gray, `[${statusCode}] ${bodyLen} (${timeTxt})`, `${cH}${cB}`.magenta);
                    },
                    OnSocketClose(u) {
                        if (crOpts.flagReverseSocketCloseLog)
                            console.log(`[${ru.tag}|${u.req.tag}]Socket Close.`.magenta, u.res.localPort, (u.req.isIndependent ? '独立的'.magenta : ''));
                    },
                });
            }
            else {
                let headers = crOpts.clientResponse.headers || {};
                let statusCode = crOpts.clientResponse.statusCode || 500;
                let rBody = crOpts.clientResponse.bodySrc;
                if (isHttps2) {
                    if (stream.writable) {
                        headers[HTTP2_HEADER_STATUS] = '' + statusCode;
                        stream.respond(headers);
                        stream.end(rBody);
                    }
                    else
                        console.error(`[${ru.tag}][Error](立即本地返回): 响应socket不可写`.red);
                }
                else {
                    if (response.socket.writable) {
                        response.writeHead(statusCode, headers);
                        if (rBody)
                            response.end(rBody);
                        else
                            response.end();
                    }
                    else {
                        console.error(`[${ru.tag}][Error](立即本地返回): 响应socket不可写`.red);
                    }
                }
            }
        }
    };
}
function ReverseResponseHeaders(opts) {
    let responseHeaders = opts.responseHeaders;
    if (typeof responseHeaders === 'object') {
        let statusCode = opts.statusCode;
        if (opts.isHttps2) {
            if (opts.stream && opts.stream.writable) {
                responseHeaders[HTTP2_HEADER_STATUS] = '' + statusCode;
                opts.stream.respond(responseHeaders);
                return true;
            }
            else {
                console.error(`[${opts.tag}][Error][H2]OnResHeaders(): 响应socket不可写`.red);
            }
        }
        else {
            if (opts.response && opts.response.socket.writable) {
                opts.response.writeHead(statusCode, responseHeaders);
                return true;
            }
            else {
                console.error(`[${opts.tag}][Error][H1]OnResHeaders(): 响应socket不可写`.red);
            }
        }
    }
    else {
        console.error(`[${opts.tag}][Error]OnResHeaders(): responseHeader内容是空的`.red);
    }
    return false;
}
