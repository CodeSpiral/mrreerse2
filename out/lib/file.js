"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Path = require("path");
const _Fs = require("fs");
const _DirBase = _Path.join(__dirname, '../..');
function ReadFileSync(file, log) {
    if (_Fs.existsSync(file)) {
        if (log)
            console.log(`[读]文件：${file.replace(_DirBase, '')}`);
        return _Fs.readFileSync(file, { encoding: 'utf8' });
    }
    else {
        return undefined;
    }
}
exports.ReadFileSync = ReadFileSync;
function ReadJsonFileSync(file, log) {
    if (_Fs.existsSync(file)) {
        if (log)
            console.log(`[读]JSON文件：${file.replace(_DirBase, '')}`);
        let txt = _Fs.readFileSync(file, { encoding: 'utf8' });
        try {
            if (txt.trim() === '')
                return undefined;
            return JSON.parse(txt);
        }
        catch (error) {
            console.warn(`[读]JSON失败！: ${file.replace(_DirBase, '')}`.red, error);
            return false;
        }
    }
    else {
        return undefined;
    }
}
exports.ReadJsonFileSync = ReadJsonFileSync;
function WriteFileJson(file, obj, log) {
    WriteFile(file, JSON.stringify(obj, null, 2), log);
}
exports.WriteFileJson = WriteFileJson;
function WriteFileJsonOnChange(file, obj, log) {
    WriteFileOnChange(file, JSON.stringify(obj, null, 2), log);
}
exports.WriteFileJsonOnChange = WriteFileJsonOnChange;
function WriteFile(file, data, log) {
    try {
        _Fs.writeFile(file, data, function (err) {
            if (err)
                console.warn('[写][Error]:'.red, err);
            else
                log && console.log(`[写]文件: ${file.replace(_DirBase, '')}`);
        });
    }
    catch (error) {
        console.warn('[写][Error]:'.red, error);
    }
}
exports.WriteFile = WriteFile;
function WriteFileOnChange(file, data, log) {
    try {
        if (!data)
            return;
        if (typeof data === 'string')
            data = Buffer.from(data);
        if (_Fs.existsSync(file)) {
            var buf = _Fs.readFileSync(file);
            if (data.length == buf.length && buf.compare(data) === 0) {
                console.log(`[写](没改变,跳过): ${file.replace(_DirBase, '')}`);
                return;
            }
        }
        else {
            CreateDirsSync(_Path.dirname(file));
        }
        WriteFile(file, data, log);
    }
    catch (error) {
        console.warn('[写][Error]:'.red, error);
    }
}
exports.WriteFileOnChange = WriteFileOnChange;
function WriteAdd(file, data, log) {
    try {
        if (!data)
            return;
        _Fs.appendFile(file, data, function (err) {
            if (err)
                console.warn('[+写][Error]:'.red, err);
            else
                log && console.log(`[+写]文件: ${file.replace(_DirBase, '')}`);
        });
    }
    catch (error) {
        console.warn('[+写][Error]:'.red, error);
    }
}
exports.WriteAdd = WriteAdd;
function CreateDirsSync(dir) {
    let paths = [];
    while (!_Fs.existsSync(dir)) {
        paths.push(dir);
        dir = _Path.dirname(dir);
    }
    for (let index = paths.length - 1; index >= 0; index--) {
        let dir = paths[index];
        _Fs.mkdirSync(dir);
    }
}
exports.CreateDirsSync = CreateDirsSync;
function WriteFileOrCreate(dir, fileName, data, log) {
    try {
        CreateDirsSync(dir);
        let file = _Path.join(dir, fileName);
        _Fs.writeFile(file, data, function (err) {
            if (err)
                console.warn('[写][Error]:'.red, err);
            else
                log && console.log(`[写]文件: ${file.replace(_DirBase, '')}`);
        });
    }
    catch (error) {
        console.warn('[写][Error]:'.red, error);
    }
}
exports.WriteFileOrCreate = WriteFileOrCreate;
function WriteAddOrCreate(dir, fileName, data, log) {
    try {
        CreateDirsSync(dir);
        let file = _Path.join(dir, fileName);
        let ws = _Fs.createWriteStream(file, { flags: 'a', }).on('error', function (err) {
            console.warn('[+写][Error]:'.red, err);
        }).end(data, function () {
            log && console.log('[+写]: ' + file);
        });
    }
    catch (error) {
        console.warn('[+写][Error]:'.red, error);
    }
}
exports.WriteAddOrCreate = WriteAddOrCreate;
