"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Net = require("net");
const _Tls = require("tls");
const _Http = require("http");
const _Https = require("https");
const _Http2 = require("http2");
const { HTTP2_HEADER_METHOD, HTTP2_HEADER_PATH, HTTP2_HEADER_STATUS, HTTP2_HEADER_CONTENT_TYPE, HTTP2_HEADER_AUTHORITY, } = _Http2.constants;
const http2ReqSocks5_1 = require("./http2ReqSocks5");
const httpReqUnit_1 = require("./httpReqUnit");
const _TAG_ = 'h2Req';
const _DEBUG_LOG_ = !true;
let _requestIndex = 0;
function GetRequestIndex() {
    _requestIndex++;
    if (_requestIndex >= 99999) {
        _requestIndex = 0;
    }
    return _requestIndex;
}
function CreateUnit(ops) {
    const httpVersion = ops.httpVersion || '2.0';
    const isHttp2 = (httpVersion == '2.0');
    const requestIndex = GetRequestIndex();
    const requestTag = `req${httpVersion[0]}-${requestIndex}` + ((ops.tunnel && ops.tunnel.use && ops.tunnel.type) ?
        ('-' + (ops.tunnel.type === 'socket5' ? 's5' : ops.tunnel.type))
        : '');
    const requestMethod = ops.method || 'GET';
    const requestHeaders = ops.headers || {};
    if (isHttp2) {
        if (!requestHeaders[HTTP2_HEADER_PATH] && ops.path)
            requestHeaders[HTTP2_HEADER_PATH] = ops.path;
        if (!requestHeaders[HTTP2_HEADER_METHOD])
            requestHeaders[HTTP2_HEADER_METHOD] = requestMethod;
    }
    const path = requestHeaders[HTTP2_HEADER_PATH] ? requestHeaders[HTTP2_HEADER_PATH] : ops.path;
    var bodyAsyncSend = false;
    if (ops.body === undefined) {
        if (ops.bodyAsyncSend === undefined)
            bodyAsyncSend = (requestMethod != 'GET');
        else
            bodyAsyncSend = ops.bodyAsyncSend;
    }
    else {
        bodyAsyncSend = false;
    }
    const reqGeter = {
        EmitOnSocketClose(socket) {
            ops.OnSocketClose && ops.OnSocketClose(unit);
        },
        EmitError(tag, error) {
            if (ops.OnError) {
                ops.OnError(unit, tag, error);
            }
            else {
                console.error(`[${unit.req.tag}][Error:${httpReqUnit_1.RequestError[tag]}]:`.red, error.message);
            }
        },
        EmitRequestErrorAndEnd(EmitError, tag, error) {
            if (EmitError)
                reqGeter.EmitError(tag, error);
            if (ops.OnRequestErrorAndEnd) {
                ops.OnRequestErrorAndEnd(unit, tag, error);
            }
            else {
                console.error(`[${unit.req.tag}][RequestErrorAndEnd:${httpReqUnit_1.RequestError[tag]}]请求操作终止:`.red, error.message);
            }
            return unit;
        },
        SetIsFinish() { unit.req.isFinish = true; },
        SetIsEnd() { unit.req.isEnd = true; },
        SetIsClose() {
            unit.req.isClose = true;
            ops.OnClose && ops.OnClose(unit);
        },
        req: {
            SetIsSendHeaders() { unit.req.isSendHeaders = true; },
            WaitSetOrSendBody(reqEnd) {
                const reqDoDnd = this.SendBody = (body) => {
                    if (unit.req.isSendBody) {
                        console.warn(`[${unit.req.tag}][warn][ unit.SendBody() ]已经SendBody了。不再发送。`.red);
                        return;
                    }
                    reqEnd(body);
                    unit.req.body = body;
                    unit.req.isSendBody = true;
                };
                _DEBUG_LOG_ && console.log(`[${unit.req.tag}][ WaitSetOrSendBody() ]method:`.magenta, requestMethod, bodyAsyncSend);
                if (bodyAsyncSend == false) {
                    _DEBUG_LOG_ && console.log(`[${unit.req.tag}][ req.end() ]method === 'GET'`.magenta);
                    if (unit.req.method === 'GET') {
                        unit.req.body && unit.req.body.length
                            && console.warn(`[${unit.req.tag}][warn]:[ req.end() ]-> GET时body数据无效。`.magenta);
                        reqDoDnd(undefined);
                    }
                    else {
                        reqDoDnd(unit.req.body);
                    }
                }
                else {
                    if (requestMethod == 'GET') {
                        _DEBUG_LOG_ && console.log(`[${unit.req.tag}][ 等待 Unit.SendBody() ]method === 'GET'`.magenta);
                        reqDoDnd(unit.req.body);
                    }
                    else {
                        _DEBUG_LOG_ && console.log(`[${unit.req.tag}][ 等待 Unit.SendBody() ]method:`.magenta, requestMethod);
                    }
                }
            },
            SendBody(body) {
                console.warn(`[${unit.req.tag}][warn][ Unit.SendBody -> reqGeter.SendBody ]方法并没有设置（未初始化）`.red);
                if (bodyAsyncSend) {
                    console.warn(`[${unit.req.tag}][warn]bodyAsyncSend=`.red, bodyAsyncSend);
                }
            },
        },
        res: {
            OnResSocket(socket) {
                unit.res.remotePort = socket.remotePort || 0;
                unit.res.localPort = socket.localPort || 0;
            },
            EmitOnResHeaders(resHeaders, statusCode) {
                unit.res.headers = resHeaders;
                if (statusCode) {
                    unit.res.statusCode = statusCode;
                }
                else {
                    unit.res.statusCode = parseInt(resHeaders[HTTP2_HEADER_STATUS]) || 0;
                }
                if (resHeaders['content-encoding'])
                    unit.res.contentEncoding = resHeaders['content-encoding'];
                ops.OnResHeaders && ops.OnResHeaders(unit, resHeaders);
            },
            EmitOnResData(chunk, data) { ops.OnResData && ops.OnResData(unit, chunk, data); },
            EmitOnResEnd(body) {
                unit.res.body = body;
                ops.OnResEnd && ops.OnResEnd(unit, unit.res, body);
            },
            GetHeaderH1() {
                const h = {};
                const reqH = unit.res.headers;
                if (reqH) {
                    for (const key in reqH) {
                        if (key && key[0] != ':')
                            h[key] = reqH[key];
                    }
                }
                return h;
            },
            GetDecodedBody(encoding) {
                if (unit.res.body === undefined)
                    return '';
                if (unit.res.contentEncoding) {
                    if (encoding)
                        return httpReqUnit_1.ZlibGunzipSync(unit.res.contentEncoding, unit.res.body).toString(encoding);
                    else
                        return httpReqUnit_1.ZlibGunzipSync(unit.res.contentEncoding, unit.res.body).toString();
                }
                else {
                    if (encoding)
                        return unit.res.body.toString(encoding);
                    else
                        return unit.res.body.toString();
                }
            },
            GetDecodedBuffer() {
                if (unit.res.body === undefined)
                    return Buffer.alloc(0);
                if (unit.res.contentEncoding) {
                    return httpReqUnit_1.ZlibGunzipSync(unit.res.contentEncoding, unit.res.body);
                }
                else {
                    return unit.res.body;
                }
            },
        }
    };
    const urlOpt = httpReqUnit_1.CreateUrlOptions(ops.authority);
    const unit = {
        options: ops,
        reqGeter,
        req: {
            index: requestIndex,
            tag: requestTag,
            isIndependent: !!ops.isIndependent,
            httpVersion: httpVersion,
            method: requestMethod,
            authority: ops.authority,
            path: path,
            headers: requestHeaders,
            body: ops.body,
            bodyAsyncSend: bodyAsyncSend,
            isSendHeaders: false,
            isSendBody: false,
            isFinish: false,
            isEnd: false,
            isClose: false,
            rejectUnauthorized: (typeof ops.rejectUnauthorized === 'boolean') ? ops.rejectUnauthorized : true,
            tunnel: ops.tunnel,
            port: urlOpt && urlOpt.port || 0,
            hostname: urlOpt && urlOpt.hostname || '',
            protocol: urlOpt && urlOpt.protocol || '',
        },
        SendBody(body) {
            reqGeter.req.SendBody(body);
        },
        res: {
            remotePort: 0,
            localPort: 0,
            statusCode: 0,
            GetHeaderH1: reqGeter.res.GetHeaderH1,
            GetDecodedBody: reqGeter.res.GetDecodedBody,
            GetDecodedBuffer: reqGeter.res.GetDecodedBuffer,
        },
    };
    return unit;
}
const _clientUnitDataSet = {
    '1.1': {},
    '2.0': {},
};
function DeleteClientUnitDataSet(authority) {
    const vl = ['1.1', '2.0'];
    vl.forEach((httpVer) => {
        for (const tag in _clientUnitDataSet[httpVer]) {
            if (tag.indexOf(authority) >= 0) {
                const tagObjList = _clientUnitDataSet[httpVer][tag];
                _clientUnitDataSet[httpVer][tag] = {};
                const tagList = Object.getOwnPropertySymbols(tagObjList);
                tagList.forEach(symbolId => {
                    delete tagObjList[symbolId];
                });
                console.log(`清空连接池[${httpVer}][${tag}]数量：${tagList.length}`.magenta);
            }
        }
    });
}
exports.DeleteClientUnitDataSet = DeleteClientUnitDataSet;
function AddClientSession(unitReq, clientSession, type) {
    const debug_log = _DEBUG_LOG_;
    if (unitReq.isIndependent)
        return undefined;
    if (!clientSession)
        return undefined;
    let tag = ((unitReq.tunnel && unitReq.tunnel.use && unitReq.tunnel.type) ? unitReq.tunnel.type + '|' : '') + unitReq.authority;
    if (!_clientUnitDataSet[unitReq.httpVersion][tag])
        _clientUnitDataSet[unitReq.httpVersion][tag] = {};
    let obj = _clientUnitDataSet[unitReq.httpVersion][tag];
    let id = Symbol(unitReq.index);
    let nowObj = obj[id] = {
        id,
        isUsing: true,
        isClose: false,
        client: clientSession,
        type: type,
        tag: tag,
        uesTunnel: unitReq.tunnel ? (unitReq.tunnel.use) : false,
    };
    debug_log && console.log(`[ClientUnit]Add [${id.toString()}] on: [${unitReq.tag}][${tag}](${nowObj.type})`.magenta);
    return nowObj;
}
function GetClientSession(unitReq) {
    const debug_log = _DEBUG_LOG_;
    let tag = ((unitReq.tunnel && unitReq.tunnel.use && unitReq.tunnel.type) ? unitReq.tunnel.type + '|' : '') + unitReq.authority;
    if (_clientUnitDataSet[unitReq.httpVersion][tag]) {
        let list = _clientUnitDataSet[unitReq.httpVersion][tag];
        let idList = Object.getOwnPropertySymbols(list);
        for (let i = 0; i < idList.length; i++) {
            const id = idList[i];
            if (list[id]) {
                const item = list[id];
                let isOK = false;
                if (item.isUsing) {
                    debug_log && console.log(`[ClientUnit]isUsing [${id.toString()}-${item.type}], close: ${item.isClose}`.magenta);
                    continue;
                }
                else if (item.client instanceof _Https.Agent) {
                    if (item.isClose) {
                        delete list[id];
                        debug_log && console.log(`[ClientUnit]delete [${id.toString()}-${item.type}](Https.Agent), close: ${item.isClose}`.magenta, {});
                        continue;
                    }
                    isOK = true;
                }
                else if (item.client instanceof _Http.Agent) {
                    isOK = true;
                }
                else if (item.isClose
                    || item.client.destroyed || !item.client.socket
                    || !item.client.socket.writable || !item.client.socket.readable) {
                    delete list[id];
                    debug_log && console.log(`[ClientUnit]delete [${id.toString()}-${item.type}], close: ${item.isClose}`.magenta);
                }
                else {
                    isOK = true;
                }
                if (isOK) {
                    item.isUsing = true;
                    debug_log && console.log(`[ClientUnit]GetClientSession(): OK [${id.toString()}-${item.type}]`.magenta);
                    return item;
                }
            }
        }
        return false;
    }
    else {
        return false;
    }
}
function Request(options) {
    const unit = CreateUnit(options);
    const unitReq = unit.req;
    const unitReqGeter = unit.reqGeter;
    if (!unitReq.authority) {
        return unitReqGeter.EmitRequestErrorAndEnd(true, httpReqUnit_1.RequestError.AuthorityError, new Error(`[${unit.req.tag}]authority是空的。`));
    }
    if (!unitReq.port || !unitReq.hostname) {
        return unitReqGeter.EmitRequestErrorAndEnd(true, httpReqUnit_1.RequestError.AuthorityError, new Error(`[${unit.req.tag}]authority解析错误。`));
    }
    if (unitReq.tunnel && unitReq.tunnel.use) {
        if (unitReq.tunnel.type === 'socket5') {
            let clientSessionUnit = unitReq.isIndependent ? false : GetClientSession(unitReq);
            if (clientSessionUnit) {
                _DEBUG_LOG_ &&
                    console.log(`[${unitReq.tag}][GetClientSession]使用：${clientSessionUnit.id.toString()}`.magenta);
                Http1OrHttps2Connect(unitReq, unitReqGeter, clientSessionUnit, undefined);
            }
            else {
                Socket5CreateConnection(unitReq.tunnel, unitReq, (socket, err, isClose) => {
                    if (err) {
                        return unitReqGeter.EmitRequestErrorAndEnd(true, httpReqUnit_1.RequestError.RequestError, new Error(`[${unit.req.tag}]socket5连接异常：${err.message}`));
                    }
                    if (socket) {
                        if (isClose || socket.writable == false || socket.destroyed) {
                            unitReqGeter.SetIsClose();
                            return unitReqGeter.EmitRequestErrorAndEnd(true, httpReqUnit_1.RequestError.RequestError, new Error(`[${unit.req.tag}]socket5连接异常并关闭。`));
                        }
                        else {
                            _DEBUG_LOG_ && console.log(`[${unit.req.tag}][socket5]socket创建完成。`.magenta);
                            Http1OrHttps2Connect(unitReq, unitReqGeter, undefined, socket);
                        }
                    }
                    else {
                        return unitReqGeter.EmitRequestErrorAndEnd(true, httpReqUnit_1.RequestError.RequestError, new Error(`[${unit.req.tag}]socket5连接异常。`));
                    }
                });
            }
        }
        else {
            return unitReqGeter.EmitRequestErrorAndEnd(true, httpReqUnit_1.RequestError.RequestError, new Error(`[${unit.req.tag}]不支持tunnel：${unitReq.tunnel.type}。`));
        }
    }
    else {
        let clientSessionUnit = unitReq.isIndependent ? false : GetClientSession(unitReq);
        if (clientSessionUnit) {
            _DEBUG_LOG_ &&
                console.log(`[${unitReq.tag}][GetClientSession]使用：${clientSessionUnit.id.toString()}`.magenta);
            Http1OrHttps2Connect(unitReq, unitReqGeter, clientSessionUnit, undefined);
        }
        else {
            Http1OrHttps2Connect(unitReq, unitReqGeter, undefined, undefined);
        }
    }
    return unit;
}
exports.Request = Request;
function Socket5CreateConnection(opt, unitReq, callback) {
    let log = !true;
    let isDoCallback = false;
    let lastError = undefined;
    let doCallback = (s, e, c) => {
        if (!isDoCallback) {
            isDoCallback = true;
            setTimeout(() => { callback(s, e, c); }, 0);
        }
    };
    const socket = _Net.createConnection(opt.port, opt.ip, () => {
        log && console.log(`[${unitReq.tag}-socket]connected!`.red, socket.readable, socket.writable);
        try {
            http2ReqSocks5_1.Socks5Link(socket, unitReq.hostname, unitReq.port, (err) => {
                log && console.log(`[${unitReq.tag}-socket][Socks5Link]callback.`.red, err ? err : '');
                if (err) {
                    socket.emit('error', err);
                    socket.emit('close', true);
                }
                else if (socket.destroyed) {
                    socket.emit('error', new Error('[Socks5Link]连接失败，socket已经关闭。'));
                    socket.emit('close', true);
                }
                else {
                    doCallback(socket, undefined, false);
                }
            });
        }
        catch (error) {
            console.log(`[${unitReq.tag}-socket]Socks5Link:error:`.red, error);
            socket.emit('error', error);
            socket.emit('close', true);
        }
    });
    socket.on('error', (err) => {
        log && console.log(`[${unitReq.tag}-socket]error!`.red, err);
        lastError = err;
    });
    socket.on('close', (has_err) => {
        log && console.log(`[${unitReq.tag}-socket]close.`.red, has_err);
        doCallback(socket, lastError, true);
    });
}
function Http1OrHttps2Connect(unitReq, unitReqGeter, clientUnit, socket) {
    if (unitReq.httpVersion === '2.0') {
        Https2Connect(unitReq, unitReqGeter, clientUnit, socket);
    }
    else if (unitReq.httpVersion === '1.1') {
        Http1Connect(unitReq, unitReqGeter, clientUnit, socket);
    }
    else {
        return unitReqGeter.EmitRequestErrorAndEnd(true, httpReqUnit_1.RequestError.RequestError, new Error(`[${unitReq.tag}]不支持httpVersion：${unitReq.httpVersion}。`));
    }
}
function Https2Connect(unitReq, unitReqGeter, clientUnit, socket) {
    _DEBUG_LOG_ && console.log(`[${unitReq.tag}]Http2Connect`.magenta, socket ? '带socket' : '');
    let isConnectOk = false;
    let lastError = undefined;
    let clientSession;
    let nowClientUnit = clientUnit ? ((!clientUnit.client.destroyed) ? clientUnit : undefined) : undefined;
    if (nowClientUnit) {
        clientSession = nowClientUnit.client;
        clientSession.removeAllListeners();
        isConnectOk = true;
        SetClientSessionEvent();
        _DEBUG_LOG_ &&
            console.log(`[${unitReq.tag}][clientSession-listener][重用]`.magenta, '写入:', clientSession.socket.writable, '认证:', (clientSession.socket instanceof _Tls.TLSSocket) ? clientSession.socket.authorized : '[isNotTls]', `本地: ${clientSession.socket.localAddress}:${clientSession.socket.localPort}`, `远程: ${clientSession.socket.remoteAddress}:${clientSession.socket.remotePort}`);
        Https2ConnectOk(unitReq, clientSession, unitReqGeter, nowClientUnit);
    }
    else {
        const connOpt = {
            rejectUnauthorized: unitReq.rejectUnauthorized,
            socket: socket,
        };
        clientSession = _Http2.connect(unitReq.authority, connOpt, function (session, socket) {
            _DEBUG_LOG_ &&
                console.log(`[${unitReq.tag}][clientSession-listener]`.magenta, '写入:', socket.writable, '认证:', (socket instanceof _Tls.TLSSocket) ? socket.authorized : '[isNotTls]', `本地: ${socket.localAddress}:${socket.localPort}`, `远程: ${socket.remoteAddress}:${socket.remotePort}`, clientSession.socket === socket, session.socket === socket, clientSession.socket === session.socket);
            if (socket.writable && socket.readable) {
                nowClientUnit = AddClientSession(unitReq, clientSession, 'ClientHttp2Session');
                isConnectOk = true;
                Https2ConnectOk(unitReq, clientSession, unitReqGeter, nowClientUnit);
            }
            else {
                lastError = new Error(`[${unitReq.tag}][clientSession-listener]socket异常：无法读写。`);
                clientSession.close();
            }
        });
        SetClientSessionEvent();
    }
    function SetClientSessionEvent() {
        clientSession.on('error', function (err) {
            if (nowClientUnit)
                nowClientUnit.isClose = true;
            lastError = err;
            console.log(`[${unitReq.tag}][clientSession-error].`.magenta);
            unitReqGeter.EmitError(httpReqUnit_1.RequestError.ClientError, err);
        });
        clientSession.on('close', function () {
            if (nowClientUnit)
                nowClientUnit.isClose = true;
            _DEBUG_LOG_ &&
                console.log(`[${unitReq.tag}][clientSession-close].`.magenta, (clientSession.socket ? '' : 'socket空的'), (clientSession.socket && `本地: ${clientSession.socket.localAddress}:${clientSession.socket.localPort}`));
            unitReqGeter.SetIsClose();
            unitReqGeter.EmitOnSocketClose(clientSession.socket);
            if (isConnectOk == false) {
                let errMessage = lastError && lastError.message;
                unitReqGeter.EmitRequestErrorAndEnd(false, httpReqUnit_1.RequestError.ClientError, new Error(`[${unitReq.tag}][Error]:${errMessage || 'Error...'}.`));
            }
        });
    }
}
function Http1Connect(unitReq, unitReqGeter, clientUnit, socket) {
    _DEBUG_LOG_ && console.log(`[${unitReq.tag}]H1ConnectInit`.magenta, socket ? '带socket' : '');
    let isConnectOk = false;
    let lastError = undefined;
    let isHttpProtocol = undefined;
    if (unitReq.protocol === 'https:') {
        isHttpProtocol = false;
    }
    else if (unitReq.protocol === 'http:') {
        isHttpProtocol = true;
    }
    else {
        return unitReqGeter.EmitRequestErrorAndEnd(true, httpReqUnit_1.RequestError.Http1RequestError, new Error(`[${unitReq.tag}]不支持protocol：${unitReq.protocol}。`));
    }
    let httpOpt = {
        hostname: unitReq.hostname,
        port: unitReq.port,
        path: unitReq.path,
        method: unitReq.method,
        headers: unitReq.headers,
        rejectUnauthorized: unitReq.rejectUnauthorized,
    };
    let isKeepAlive = unitReq.headers['connection'] == 'keep-alive';
    let _agent;
    let useClientUnit = false;
    if (!unitReq.isIndependent && isKeepAlive) {
        if (clientUnit && !clientUnit.isClose) {
            useClientUnit = true;
            httpOpt.agent = _agent = clientUnit.client;
        }
        else {
            if (isHttpProtocol) {
                httpOpt.agent = _agent = new _Http.Agent({ keepAlive: true });
            }
            else {
                httpOpt.agent = _agent = new _Https.Agent({
                    rejectUnauthorized: unitReq.rejectUnauthorized,
                    keepAlive: isKeepAlive,
                    socket: socket
                });
            }
        }
    }
    const request = isHttpProtocol ? _Http.request : _Https.request;
    const httpClientRequest = request(httpOpt, (res) => {
        _DEBUG_LOG_ &&
            console.log(`[${unitReq.tag}][http-listener]`.magenta, (unitReq.tunnel && unitReq.tunnel.use), '写入:', httpClientRequest.socket.writable, `本地: ${httpClientRequest.socket.localAddress}:${httpClientRequest.socket.localPort}`, `远程: ${httpClientRequest.socket.remoteAddress}:${httpClientRequest.socket.remotePort}`, httpClientRequest.socket === res.socket);
        unitReqGeter.res.OnResSocket(httpClientRequest.socket);
        unitReqGeter.res.EmitOnResHeaders(res.headers, res.statusCode);
        if (httpClientRequest.socket.writable && httpClientRequest.socket.readable) {
            clientUnit = ((false == useClientUnit) && _agent)
                ? AddClientSession(unitReq, _agent, isHttpProtocol ? 'Http.Agent' : 'Https.Agent')
                : clientUnit;
            isConnectOk = true;
            Https2Http1ConnectOk(res, unitReq, unitReqGeter, clientUnit);
        }
        else {
            lastError = new Error(`[${unitReq.tag}][http-listener]socket异常：无法读写。`);
            httpClientRequest.destroy();
        }
    });
    unitReqGeter.req.SetIsSendHeaders();
    httpClientRequest.on('socket', (socket) => {
        socket.removeAllListeners('error');
        socket.removeAllListeners('close');
        socket.on('error', function (err) {
            if (clientUnit)
                clientUnit.isClose = true;
            lastError = err;
            _DEBUG_LOG_ && console.log(`[${unitReq.tag}][Socket-error].`.magenta);
        });
        socket.on('close', (had_err) => {
            if (clientUnit)
                clientUnit.isClose = true;
            _DEBUG_LOG_ &&
                console.log(`[${unitReq.tag}][Socket-close].`.magenta, `本地: ${httpClientRequest.socket.localAddress}:${httpClientRequest.socket.localPort}`, `远程: ${httpClientRequest.socket.remoteAddress}:${httpClientRequest.socket.remotePort}`);
            httpClientRequest.emit('close');
            unitReqGeter.SetIsClose();
            unitReqGeter.EmitOnSocketClose(socket);
        });
    });
    httpClientRequest.on('error', function (err) {
        if (clientUnit)
            clientUnit.isClose = true;
        lastError = err;
        _DEBUG_LOG_ && console.log(`[${unitReq.tag}][ClientRequest-error].`.magenta);
        unitReqGeter.EmitError(httpReqUnit_1.RequestError.ClientError, err);
    });
    httpClientRequest.on('close', function () {
        if (clientUnit)
            clientUnit.isClose = true;
        _DEBUG_LOG_ &&
            console.log(`[${unitReq.tag}][ClientRequest-close].`.magenta, httpClientRequest.socket.localAddress);
        unitReqGeter.SetIsClose();
        if (isConnectOk == false) {
            let errMessage = lastError && lastError.message;
            unitReqGeter.EmitRequestErrorAndEnd(false, httpReqUnit_1.RequestError.ClientError, new Error(`[${unitReq.tag}][Error]:${errMessage || 'Error...'}.`));
        }
    });
    httpClientRequest.on('finish', () => {
        _DEBUG_LOG_ && console.log(`[${unitReq.tag}][finish]请求完成时触发.`.magenta);
        unitReqGeter.SetIsFinish();
    });
    unitReqGeter.req.WaitSetOrSendBody((body) => { httpClientRequest.writable && httpClientRequest.end(body); });
}
function Https2ConnectOk(unitReq, client, unitReqGeter, clientSessionUnit) {
    const req = client.request(unitReq.headers);
    unitReqGeter.req.SetIsSendHeaders();
    req.on('response', (headers, flags) => {
        _DEBUG_LOG_ && console.log(`[${unitReq.tag}][response(headers)]响应头:`, headers);
        _DEBUG_LOG_ && console.log(`[${unitReq.tag}][response]`.magenta, (unitReq.tunnel && unitReq.tunnel.use), 'writable:', client.socket.writable, 'authorized:', (client.socket instanceof _Tls.TLSSocket) ? client.socket.authorized : '[isNotTls]', `${client.socket.localAddress}:${client.socket.localPort}`, `${client.socket.remoteAddress}:${client.socket.remotePort}`);
        unitReqGeter.res.OnResSocket(client.socket);
        unitReqGeter.res.EmitOnResHeaders(headers, undefined);
    });
    Https2Http1ConnectOk(req, unitReq, unitReqGeter, clientSessionUnit);
    unitReqGeter.req.WaitSetOrSendBody((body) => { req.writable && req.end(body); });
}
function Https2Http1ConnectOk(req, unitReq, unitReqGeter, clientSessionUnit) {
    let data = Buffer.alloc(0);
    req.on('data', (chunk) => {
        data = Buffer.concat([data, chunk]);
        unitReqGeter.res.EmitOnResData(chunk, data);
    });
    req.on('end', () => {
        _DEBUG_LOG_ && console.log(`[${unitReq.tag}][end]${clientSessionUnit ? '(有用连接池)' : ''}响应结束:`.magenta, data.length);
        unitReqGeter.SetIsEnd();
        if (clientSessionUnit && clientSessionUnit.type != 'ClientHttp2Session') {
            clientSessionUnit.isUsing = false;
            _DEBUG_LOG_ &&
                console.log(`[${unitReq.tag}][end]响应结束: [${clientSessionUnit.id.toString()}](${clientSessionUnit.isClose}).isUsing = false;`.red);
        }
        setTimeout(unitReqGeter.res.EmitOnResEnd, 0, data);
    });
    req.on('close', () => {
        _DEBUG_LOG_ && console.log(`[${unitReq.tag}][close]连接关闭.`.magenta);
        unitReqGeter.SetIsClose();
        if (clientSessionUnit) {
            clientSessionUnit.isUsing = false;
            _DEBUG_LOG_ &&
                console.log(`[${unitReq.tag}][close]连接关闭: [${clientSessionUnit.id.toString()}](${clientSessionUnit.isClose}).isUsing = false;`.red);
        }
    });
    req.on('finish', () => {
        _DEBUG_LOG_ && console.log(`[${unitReq.tag}][finish]请求完成时触发.`.magenta);
        unitReqGeter.SetIsFinish();
    });
    req.on('error', (err) => unitReqGeter.EmitError(httpReqUnit_1.RequestError.ResponseError, err));
}
