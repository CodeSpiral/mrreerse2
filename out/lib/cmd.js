"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Colors = require("colors");
const _Readline = require("readline");
_Colors.black;
const _Cmds = [
    {
        cmd: ['h', 'help'],
        txt: '显示Cmd命令信息。', func: CmdHelp,
    },
];
function Add(cmds) {
    if (cmds) {
        for (let index = 0; index < cmds.length; index++) {
            let opt = cmds[index];
            let optCmd = opt.cmd;
            let isHas = false;
            if (typeof (optCmd) === 'string')
                isHas = CmdCheck(optCmd);
            else if (optCmd instanceof Array) {
                for (const key in optCmd)
                    isHas = CmdCheck(optCmd[key]);
            }
            if (isHas) {
                console.log(`已经存在命令：${isHas.txt}[${isHas.cmd}]。`.red, optCmd);
            }
            _Cmds.push(cmds[index]);
        }
    }
}
exports.Add = Add;
function CmdHelp() {
    console.log('Cmd（区分大小写）:');
    _Cmds.forEach(function (item) {
        console.log(`  [ ${(item.cmd instanceof Array) ? item.cmd.join(', ') : item.cmd} ]: ${item.txt}`);
    });
}
exports.CmdHelp = CmdHelp;
var _ReadLineRun;
function Start(showHelp, overCb) {
    _ReadLineRun = _Readline.createInterface(process.stdin);
    _ReadLineRun.setPrompt('');
    _ReadLineRun.prompt();
    _ReadLineRun.on('line', CmdIn);
    _ReadLineRun.on('close', function () {
        console.log('程序关闭!');
        process.exit(0);
    });
    if (showHelp)
        setTimeout(() => { CmdHelp(); }, 0);
    overCb && setTimeout(overCb, 1);
}
exports.Start = Start;
function CmdIn(line) {
    const inCmd = line.trim();
    if (!inCmd)
        return;
    if (CmdInRun(inCmd) == false)
        console.log('无该命令。');
    setTimeout(() => { _ReadLineRun.prompt(); }, 1);
}
function CmdInRun(cmd) {
    let cmdL = cmd.split(' ');
    if (!cmdL || cmdL.length <= 0)
        return false;
    let opt = CmdCheck(cmdL[0]);
    if (opt) {
        console.log(`> 运行：[ ${cmd} ]=> `, opt.txt.blue);
        setTimeout((opt, args) => { opt.func(args); console.log(); }, 1, opt, cmdL.slice(1));
        return true;
    }
    return false;
}
function CmdCheck(cmd) {
    const oneCmd = cmd.trim();
    if (!oneCmd)
        return false;
    for (let index = 0; index < _Cmds.length; index++) {
        let item = _Cmds[index];
        let cmd = item.cmd;
        let isThis = false;
        if (typeof (cmd) === 'string')
            isThis = (oneCmd == cmd);
        else if (cmd instanceof Array)
            isThis = (cmd.indexOf(oneCmd) >= 0);
        if (isThis)
            return item;
    }
    return false;
}
function RunCmd(cmd) { return CmdInRun(cmd); }
exports.RunCmd = RunCmd;
