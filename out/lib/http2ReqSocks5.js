"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Net = require("net");
function Socks5Link(socket, host, port, cb) {
    Socks5Authenticate(socket, (err) => {
        try {
            if (err) {
                setTimeout(() => { cb(err); }, 0);
            }
            else {
                socks5Connect(socket, host, port, (err) => {
                    if (err)
                        setTimeout(() => { cb(err); }, 0);
                    else
                        setTimeout(() => { cb(undefined); }, 0);
                });
            }
        }
        catch (error) {
            setTimeout(() => { cb(err); }, 0);
        }
    });
}
exports.Socks5Link = Socks5Link;
function Socks5Authenticate(socket, cb) {
    const methods = 0x00;
    const buffer = Buffer.alloc(3);
    buffer[0] = 0x05;
    buffer[1] = 0x01;
    buffer[2] = methods;
    socket.once('data', function (data) {
        var error;
        if (2 !== data.length) {
            error = '接收的数据长度异常。';
        }
        else if (0x05 !== data[0]) {
            error = '不支持的SOCKS版本：' + data[0] + '。';
        }
        else if (0xFF === data[1]) {
            error = '没有可使用的认证方式。';
        }
        else if (methods !== data[1]) {
            error = '不支持该Socks5认证方式：' + data[1] + '。';
        }
        if (error) {
            cb(new Error('认证失败：' + error));
        }
        else {
            cb(undefined);
        }
    });
    socket.write(buffer);
}
function socks5Connect(socket, host, port, cb) {
    let request = [];
    request.push(0x05);
    request.push(0x01);
    request.push(0x00);
    switch (_Net.isIP(host)) {
        case 0:
            request.push(0x03);
            let b = Buffer.from(host);
            request.push(b.length);
            for (let i = 0; i < b.length; i++) {
                request.push(b[i]);
            }
            break;
        case 4:
            request.push(0x01);
            let groups = host.split('.');
            for (let i = 0; i < 4; i++) {
                request.push(parseInt(groups[i], 10));
            }
            break;
        case 6:
            request.push(0x04);
            cb(new Error('本程序暂时不支持IPv6地址处理.'));
            return;
            break;
        default:
            cb(new Error('错误：host地址处理失败：' + host));
            return;
    }
    request.length += 2;
    let buffer = Buffer.from(request);
    buffer.writeUInt16BE(port, buffer.length - 2);
    socket.once('data', function (data) {
        var error;
        if (data[0] !== 0x05) {
            error = '不支持的SOCKS版本：' + data[0] + '。';
        }
        else if (data[1] !== 0x00) {
            switch (data[1]) {
                case 1:
                    error = '普通SOCKS服务器连接失败';
                case 2:
                    error = '现有规则不允许连接';
                case 3:
                    error = '网络无法访问';
                case 4:
                    error = '主机无法访问';
                case 5:
                    error = '连接被拒绝';
                case 6:
                    error = 'TTL超时';
                case 7:
                    error = '不支持的命令';
                case 8:
                    error = '不支持的地址类型';
                default:
                    error = '未定义的CODE：' + data[1];
            }
        }
        else if (data[2] !== 0x00) {
            error = 'RSV（保留位）不是0x00。';
        }
        if (error) {
            cb(new Error('Socks5连接失败：' + error));
            return;
        }
        else {
            cb(undefined);
        }
    });
    socket.write(buffer);
}
