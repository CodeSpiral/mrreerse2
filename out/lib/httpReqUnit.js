"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Zlib = require("zlib");
;
var RequestError;
(function (RequestError) {
    RequestError[RequestError["Error"] = 0] = "Error";
    RequestError[RequestError["AuthorityError"] = 1] = "AuthorityError";
    RequestError[RequestError["RequestError"] = 2] = "RequestError";
    RequestError[RequestError["Http1RequestError"] = 3] = "Http1RequestError";
    RequestError[RequestError["ClientError"] = 4] = "ClientError";
    RequestError[RequestError["ResponseError"] = 5] = "ResponseError";
})(RequestError = exports.RequestError || (exports.RequestError = {}));
function CreateUrlOptions(url, search) {
    if (!url)
        return undefined;
    try {
        const urlObj = new URL(url);
        if (search)
            for (const key in search) {
                urlObj.searchParams.append(key, ('' + search[key]));
            }
        let port;
        if (urlObj.port)
            port = parseInt(urlObj.port);
        else {
            switch (urlObj.protocol) {
                case 'http:':
                    port = 80;
                    break;
                case 'https:':
                    port = 443;
                    break;
                default:
                    port = 80;
                    break;
            }
        }
        return {
            protocol: urlObj.protocol,
            port: port,
            origin: urlObj.origin,
            host: urlObj.host,
            hostname: urlObj.hostname,
            pathname: urlObj.pathname,
            search: urlObj.search,
            path: urlObj.pathname + (urlObj.search || '')
        };
    }
    catch (error) {
        console.error(`[CreateUrlOptions]ERROR:`.red, error);
        return undefined;
    }
}
exports.CreateUrlOptions = CreateUrlOptions;
exports._Buffer_NULL_ = Buffer.alloc(0);
function ZlibGunzipSync(zlibType, body) {
    if (!body)
        return exports._Buffer_NULL_;
    switch (zlibType) {
        case 'gzip':
            return _Zlib.gunzipSync(body);
        default:
            console.log('[W]不明的压缩类型：'.red, zlibType);
    }
    return body;
}
exports.ZlibGunzipSync = ZlibGunzipSync;
