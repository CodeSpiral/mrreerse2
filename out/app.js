"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _App = require("./config");
const _AppConf = _App.Conf;
const _RS = require("./_reverse/server");
const _Mr = require("./mr");
const _isDev = _App._IsDev;
const _isLog = _AppConf.ReerseServerLog;
let _extFlag = false;
function SetHostExtFlag(value) {
    _extFlag = value;
    if (_extFlag) {
        console.log('支持`127.0.0.1`域名处理');
    }
}
exports.SetHostExtFlag = SetHostExtFlag;
const _hostList = [];
let _isRun = false;
function init() {
    _isRun = true;
    if (_AppConf.ReerseServer) {
        let dcList = [];
        const domainConf = _AppConf.DomainConf;
        for (let index = 0; index < domainConf.length; index++) {
            const item = domainConf[index];
            if (item.use) {
                for (let indexD = 0; indexD < item.domain.length; indexD++) {
                    const d = item.domain[indexD];
                    _hostList.push(d);
                    if (index > 0 && item.certPem && item.certKeyPem) {
                        dcList.push({
                            domain: d,
                            cert: item.certPem,
                            key: item.certKeyPem,
                        });
                    }
                }
            }
        }
        const unit = _RS.CreateHttp2ReverseServer(dcList, {
            cert: domainConf[0].certPem,
            key: domainConf[0].certKeyPem,
            listenPort: _AppConf.ReerseServerPort,
            isLog: _isLog,
            isReerseMultiUser: _AppConf.ReerseMultiUser,
            tunnel: {
                use() { return _AppConf.UseSocket5; },
                tunnelPort: _AppConf.Socket5Port,
            }
        });
        if (typeof unit === 'string') {
            console.log(`[createHttp2ReverseServer]` + unit.bgYellow.red);
            return;
        }
        else {
            const _TAG_ = 'RS';
            unit.OnRequest = (ru, opts) => {
                let urlPathSrc = ru.clientReq.pathSrc;
                let host = ru.clientReq.hostname;
                let stop = false;
                if (_hostList.indexOf(host) == -1)
                    stop = true;
                if (_extFlag && host.indexOf('127.0.0.1') >= 0)
                    stop = false;
                if (!urlPathSrc || urlPathSrc === '/')
                    stop = true;
                if (stop) {
                    console.log(`+> 域名[${host}]不工作: ${urlPathSrc}`.red);
                    opts.doReverse = false;
                    opts.clientResponse.headers = { 'pragma': 'no-cache', 'cache-control': 'no-cache' };
                    opts.clientResponse.statusCode = 500;
                    return opts;
                }
                const geterClientReq = {
                    GetClientReqHeaders() {
                        return ru.clientReq.headers;
                    },
                    _decodedBody: undefined,
                    GetClientReqDecodedBody() {
                        if (undefined === this._decodedBody)
                            this._decodedBody = opts.GetClientReqDecodedBody();
                        return this._decodedBody;
                    }
                };
                const mu = _Mr.CreateMrUnit(ru, urlPathSrc || '', ru.clientReq.pathParse, geterClientReq, ru.clientReq.method);
                const reverseOpts = mu.reverseOpts;
                const returnOpt = mu.returnOpt;
                let CustomizedResponse_Aborte = false;
                if (mu.handleConf.doReqHeaders.length > 0) {
                    for (let index = 0; index < mu.handleConf.doReqHeaders.length; index++) {
                        let handle = mu.handleConf.doReqHeaders[index];
                        let r = handle(mu, geterClientReq.GetClientReqHeaders());
                        if (r === 1) {
                            CustomizedResponse_Aborte = true;
                            mu.returnMode = 1;
                            break;
                        }
                        else if (r === 2) {
                            mu.returnOpt.customizedBody = true;
                        }
                    }
                }
                if (false == CustomizedResponse_Aborte && mu.handleConf.doReqBody.length > 0) {
                    const reqDecodedBody = geterClientReq.GetClientReqDecodedBody();
                    for (let index = 0; index < mu.handleConf.doReqBody.length; index++) {
                        let handle = mu.handleConf.doReqBody[index];
                        let r = handle(mu, reqDecodedBody);
                        if (r === 1) {
                            CustomizedResponse_Aborte = true;
                            mu.returnMode = 1;
                            break;
                        }
                        else if (r === 2) {
                            mu.returnOpt.customizedBody = true;
                        }
                        else {
                            opts.doReverse = true;
                        }
                    }
                }
                if (CustomizedResponse_Aborte || mu.returnMode === 1) {
                    opts.doReverse = false;
                    (!returnOpt.headers) && console.log(`直接响应[headers]无数据。`);
                    (!returnOpt.statusCode) && console.log(`直接响应[statusCode]无数据。`);
                    (!returnOpt.decodedBody) && console.log(`直接响应[decodedBody]无数据。`);
                    opts.clientResponse.headers = returnOpt.headers || {};
                    opts.clientResponse.statusCode = returnOpt.statusCode || 500;
                    opts.clientResponse.bodySrc = returnOpt.decodedBody || Buffer.alloc(0);
                    if (reverseOpts.notSendServerShowLog) {
                        console.log(`<- [${ru.tag}]直接返回.`.yellow, (opts.clientResponse.bodySrc.length));
                    }
                    return;
                }
                opts.doReverse = true;
                if (reverseOpts.revIsNewConnect)
                    opts.reverseReq.canNewConnect = true;
                if (reverseOpts.revClearReqUnitDataSet)
                    opts.reverseReq.clearReqUnitDataSet = true;
                if (reverseOpts.reques.headers)
                    opts.reverseReq.headers = reverseOpts.reques.headers;
                if (reverseOpts.reques.decodedBody)
                    opts.reverseReq.bodySrc = reverseOpts.reques.decodedBody;
                if (false == CustomizedResponse_Aborte && mu.handleConf.doResponseHeaders.length > 0) {
                    opts.OnResponseHeader = (s_statusCode, s_resHeaders) => {
                        mu._res.headers = s_resHeaders;
                        mu._res.statusCode = s_statusCode;
                        for (let index = 0; index < mu.handleConf.doResponseHeaders.length; index++) {
                            const handle = mu.handleConf.doResponseHeaders[index];
                            const r = handle(mu, s_resHeaders, s_statusCode);
                            if (r === 1) {
                                CustomizedResponse_Aborte = true;
                                mu.returnMode = 2;
                                break;
                            }
                            else if (r === 2) {
                                mu.returnMode = 2;
                                mu.returnOpt.customizedBody = true;
                            }
                        }
                        if (mu.returnOpt.customizedBody) {
                            opts.isWaitEndResponseBody = true;
                        }
                        else {
                            if (mu.returnMode == 2) {
                                mu.returnOpt.statusCode && (opts.clientResponse.statusCode = mu.returnOpt.statusCode);
                                mu.returnOpt.headers && (opts.clientResponse.headers = mu.returnOpt.headers);
                            }
                        }
                    };
                }
                opts.OnResponseBody = (opts, decodedBody) => {
                    mu._res.decodedBody = decodedBody;
                    mu._res.headers = ru.reverseResponse.headers;
                    mu._res.statusCode = ru.reverseResponse.statusCode;
                    if (decodedBody.length > 0 || mu._res.statusCode === 304) {
                        const host = ru.clientReq.hostname;
                        if (host && host.indexOf('.magi-reco.com') >= 0) {
                            _Mr.asyncMrHandle(mu);
                        }
                    }
                    if (false == CustomizedResponse_Aborte && mu.handleConf.doResponseBody.length > 0) {
                        for (let index = 0; index < mu.handleConf.doResponseBody.length; index++) {
                            const handle = mu.handleConf.doResponseBody[index];
                            const r = handle(mu, decodedBody);
                            if (r === 1) {
                                mu.returnMode = 2;
                                CustomizedResponse_Aborte = true;
                                break;
                            }
                            else if (r === 2) {
                                mu.returnMode = 2;
                                mu.returnOpt.customizedBody = true;
                                if (!mu.returnOpt.decodedBody) {
                                    console.log(`[${_TAG_}].doResponseBody over[ResponseBody]:但是没有设置响应body！`.magenta);
                                }
                            }
                            else if (r === 0) {
                                opts.clientResponse.responseBodyNullWarnLog = false;
                            }
                        }
                        if (mu.returnMode == 2) {
                            mu.returnOpt.statusCode && (opts.clientResponse.statusCode = mu.returnOpt.statusCode);
                            mu.returnOpt.headers && (opts.clientResponse.headers = mu.returnOpt.headers);
                            if (mu.returnOpt.decodedBody) {
                                opts.clientResponse.bodySrc = mu.returnOpt.decodedBody;
                            }
                        }
                    }
                    delete mu.handleConf;
                };
            };
            unit.Start();
        }
    }
}
if (!_isDev)
    init();
function Start() {
    if (_isRun) {
        console.log('服务已经运行了...');
        return;
    }
    init();
}
exports.Start = Start;
